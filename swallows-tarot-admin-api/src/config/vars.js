const path = require('path');

// import .env variables
require('dotenv-safe').load({
  path: path.join(__dirname, '../../.env'),
  sample: path.join(__dirname, '../../.env.example'),
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,
  databaseMysql: {
    host: process.env.HOST_MYSQL,
    user: process.env.USERNAME_DATABASE,
    password: process.env.PASSWORD_DATABASE,
    database: process.env.MYSQL_NAME_DATABASE,
  },
  mongo: {
    uri: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TESTS : process.env.MONGO_URI,
  },
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
  emailConfig: {
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    username: process.env.EMAIL_USERNAME,
    password: process.env.EMAIL_PASSWORD,
  },
  roles: {
    USER: 1,
    READER: 2,
    ADMIN: 3
  },
  cardDeckStatus: {
    processing: "PROCESSING",
    approved: "APPROVED"
  },
  language: {
    en: 'en',
    vi: 'vi'
  },
  userDefault: {
    fullname: 'super admin',
    user_id: 96
  },
  suits: {
    major: 1,
    wands: 2,
    cups: 3,
    swords: 4,
    pentacles: 5,
  }
};
