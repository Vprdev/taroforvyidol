const mysql = require('mysql');
const {databaseMysql} = require('./vars.js')

const dbConn = mysql.createConnection(databaseMysql);

dbConn.connect((err) => {
  if (err) throw err;
  console.log('Database Connected!');
});
module.exports = dbConn;

