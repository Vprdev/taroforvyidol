
const AWS = require('aws-sdk');

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SCRET_KEY
});

const S3PATH = {
  card_deck: 'card_deck/'
}
exports.AWS = AWS;
exports.S3PATH = S3PATH;

exports.S3URL = `https://${process.env.AWS_BUCKET}.s3.${process.env.AWS_REGION}.amazonaws.com/`

exports.getFullUrl = function(path = '', name = '') {
  return `https://${process.env.AWS_BUCKET}.s3.${process.env.AWS_REGION}.amazonaws.com/${path}${name}`;
}
