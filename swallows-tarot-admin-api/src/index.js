// make bluebird default Promise
Promise = require('bluebird'); // eslint-disable-line no-global-assign
const { port, env } = require('./config/vars');
const logger = require('./config/logger');
const app = require('./config/express');
const server = require('http').createServer(app);
const ChatService = require('./api/services/chat.service');
const mongoose = require('./config/mongoose');


// open mongoose connection
mongoose.connect();


//Socket logic
const io = require('socket.io')(server, {
  cors: {
    // origin: ["http://socketserve.io", 'http://localhost:8000'],
    origin: ['http://127.0.0.1:8000'],
    methods: ["GET", "POST"],
    credentials: true
  }
});

io.use(async (socket, next) => {
  try {
    const resultToken = socket.handshake?.auth?.token;
    if(resultToken) {
      const userID = ChatService.getuserID(resultToken);
      const userName =await ChatService.getUserName(userID);
      socket.userID = userID;
      socket.username = userName;
      return next();
    } else {
      console.log("require token!");
    }
    next();
  }catch(e) {
    console.log("login required!", e);
  }
})

io.on('connection', (socket) => {
  console.log("=> " + socket.username + ' connected');
  ChatService.handleSocket(io, socket);
  socket.on('disconnect', () => {
    console.log('=> ' + socket.username + ' disconnected');
  });
});


// listen to requests
server.listen(port, () => logger.info(`server started on port ${port} (${env})`));


/**
* Exports express
* @public
*/
module.exports = server;