const axios = require('axios')
axios.defaults.baseURL = process.env.APP_API_URL;

const getApiKey = function() {
  return process.env.APP_API_KEY
}

module.exports.getToAppApi = async function(url, params) {
  try {
    params.api_key = getApiKey()
		const response = await axios.get(url, { params });
		return response.data;
	} catch (e) {
		console.log('Axios Methods GET --->', { ...e });
	}
}

module.exports.postToAppApi = async function (url, data, query) {
  try {
    query.api_key = getApiKey()
		const response = await axios.post(url,data, {params: query});
		return response.data;
	} catch (e) {
		console.log('Axios Methods POST --->', { ...e });
	}
}

module.exports.putToAppApi = async function(url, data, query) {
	try {
    query.api_key = getApiKey()
		const response = await axios.put(url, data, {params: query});
		return response.data;
	} catch (e) {
		console.log('Axios Methods PUT --->', { ...e});
	}
}

module.exports.removeToAppApi = async function(url, query) {
	try {
    query.api_key = getApiKey()
		const response = await axios.delete(url, {params: query});
		return response.data;
	} catch (e) {
		console.log('Axios Methods DELETE --->', { ...e });
	}
}

module.exports.patchToAppApi = async function(url, data) {
	try {
		const response = await axios.patch(url, data);
		return response.data;
	} catch (e) {
		console.log('methods PATCH --->', { ...e });
	}
}
