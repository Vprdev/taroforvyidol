exports.successResponse = function (res, data) {
  const result = {
    message: 'SUCCESS', error: null,
    data
  };
  return res.status(200).json(result);
}

exports.successWithMessages = function (res) {
  const result = {
    message: 'SUCCESS', error: null,
    data: null
  };
  return res.status(200).json(result);
}

exports.duplicateRecord = function (res) {
  return res.status(409)
    .json({ status: false, message: 'DUPLICATE_RECORD', data: null, error: null })
}

exports.notFoundRecord = function (res) {
  return res.status(404)
    .json({ status: false, message: 'NOTFOUND_RECORD', data: null, error: null })
}

exports.customReponseNoData = function (res, code, message) {
  return res.status(code)
    .json({ status: false, message, data: null, error: null })
}
exports.customBadRequest = function (res, data) {
  return res.status(400)
    .json({ code: 400, "message": "Validation Error",  errors: data})
}
exports.errorServerInternal = function (res, data) {
  return res.status(500)
    .json({ code: 500, "message": "Server internal",  errors: data})
}
