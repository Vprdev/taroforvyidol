const _ = require('lodash');
const CardDeckSQL = require('../modelTaro/card_deck');
const CardDeck = require('../models/card_deck.model')
const Deck = require('../models/deck.model')
const DeckSQL = require('../modelTaro/deck');
const { getFullUrl } = require('../../config/aws.js')
const { language, suits } = require('../../config/vars');

exports.getListCardDecks = async (req) => {
  let conditions = [];
  if (req.query.deck) {
    conditions = [
      ...conditions,
      {
        field: 'decks.slug',
        operator: '=',
        binding: '"' + req.query.deck + '"',
      }
    ]
  }
  if (req.query.keywords) {
    conditions = [
      ...conditions,
      {
        field: 'card_decks.name',
        operator: 'LIKE',
        binding: `'%${req.query.keywords}%'`,
      }
    ]
  }

  if (req.query.suit) {
    conditions = [
      ...conditions,
      {
        field: 'cards.suit_id',
        operator: '=',
        binding: suits[req.query.suit.toLowerCase()] ?? 1,
      }
    ]
  }
  const params = {
    ...req.query,
    conditions,
    columns: [
      'card_decks.id',
      'card_decks.name',
      'card_decks.deck_id',
      'card_decks.slug',
      'card_decks.meaning',
      'card_decks.keywords',
      'card_decks.created_at',
      'card_id',
      `CONCAT('${getFullUrl('card_deck/')}', images.path) AS full_url`,
      'cards.suit_id',
      'decks.name AS deck_name'
    ],
  }
  const [res, total] = await Promise.all([
    CardDeckSQL.getList(params),
    CardDeckSQL.getCountTotal(params)
  ]);
  res.map(cardDeck => {
    cardDeck.meaning = JSON.parse(cardDeck.meaning);
    cardDeck.keywords = JSON.parse(cardDeck.keywords);
  })

  return {
    data: res,
    total: total[0]?.total ?? 0
  };
}

exports.getListDecks = async (req) => {
  let conditions = [];
  if (req.query.keywords) {
    conditions = [
      ...conditions,
      {
        field: 'decks.name',
        operator: 'LIKE',
        binding: `'%${req.query.keywords}%'`,
      }
    ]
  }
  const params = {
    ...req.query,
    conditions,
    columns: [
      'decks.id',
      'decks.name',
      'decks.slug',
      'decks.source',
      'decks.description',
      'decks.created_at',
      'decks.updated_at',
      'decks.author',
      `CONCAT('${getFullUrl('card_deck/')}', images.path) AS full_url`,
    ],
  }
  const [res, total] = await Promise.all([
    DeckSQL.getList(params),
    DeckSQL.getCountTotal(params)
  ]);
  return {
    data: res,
    total: total[0]?.total ?? 0
  };
}


exports.searchListDecks = async (req) => {
  let conditions = [];
  if (req.query.keywords) {
    conditions = [
      ...conditions,
      {
        field: 'decks.name',
        operator: 'LIKE',
        binding: `'%${req.query.keywords}%'`,
      }
    ]
  }
  const params = {
    conditions,
    columns: [
      'decks.id',
      'decks.name',
      'decks.slug',
    ],
  }
  const res = await DeckSQL.searchList(params)
  return res
}

exports.cardVersionById = async (req) => {
  const conditions = [
    {
      field: 'card_decks.id',
      operator: '=',
      binding: req.params.id,
    }
  ]
  const params = {
    conditions,
    columns: [
      'card_decks.id',
      'card_decks.name',
      'card_decks.deck_id',
      'card_decks.slug',
      'card_decks.meaning',
      'card_decks.keywords',
      'card_decks.created_at',
      'card_id',
      `CONCAT('${getFullUrl('card_deck/')}', images.path) AS full_url`,
      'cards.suit_id',
      'decks.name AS deck_name'
    ],
  }

  const card = await CardDeckSQL.findBy(params)
  if (!_.isEmpty(card)) {
    card.map(cardDeck => {
      cardDeck.meaning = JSON.parse(cardDeck.meaning);
      cardDeck.keywords = JSON.parse(cardDeck.keywords);
    })
    //Get same card from mongo by mysql card deck name
    const currentDeckMongo = await Deck.findOne({ name: card[0].deck_name }).lean();
    //Get main EL version card
    const mainELVersionMongo = await CardDeck.findOne({ deck_id: currentDeckMongo._id, name: card[0].name, language: language.en }).lean();
    const { group_id } = mainELVersionMongo;
    const viCardDeckVersion = await CardDeck.findOne({ group_id, language: language.vi }).lean(); // Curently, only having vi version
    return {
      en: mainELVersionMongo ?? null,
      vi: viCardDeckVersion ?? null,
    }
  }
  return card;
}

exports.cardVersionBySlug = async (cardSlug) => {
  const conditions = [
    {
      field: 'card_decks.slug',
      operator: '=',
      binding: '"' + cardSlug + '"',
    }
  ]
  const params = {
    conditions,
    columns: [
      'card_decks.id',
      'card_decks.name',
      'card_decks.deck_id',
      'card_decks.slug',
      'card_decks.meaning',
      'card_decks.keywords',
      'card_decks.created_at',
      'card_id',
      `CONCAT('${getFullUrl('card_deck/')}', images.path) AS full_url`,
      'cards.suit_id',
      'decks.name AS deck_name'
    ],
  }

  const card = await CardDeckSQL.findBy(params)
  if (!_.isEmpty(card)) {
    card.map(cardDeck => {
      cardDeck.meaning = JSON.parse(cardDeck.meaning);
      cardDeck.keywords = JSON.parse(cardDeck.keywords);
    })
    //Get same card from mongo by mysql card deck name
    const currentDeckMongo = await Deck.findOne({ name: card[0].deck_name }).lean();
    //Get main EL version card
    const mainELVersionMongo = await CardDeck.findOne({ deck_id: currentDeckMongo._id, name: card[0].name, language: language.en }).lean();
    const { group_id } = mainELVersionMongo;
    if (mainELVersionMongo) {
      mainELVersionMongo.full_url = getFullUrl('card_deck/', mainELVersionMongo.url);
    }
    const viCardDeckVersion = await CardDeck.findOne({ group_id, language: language.vi }).lean(); // Curently, only having vi version
    if (viCardDeckVersion) {
      viCardDeckVersion.full_url = getFullUrl('card_deck/', viCardDeckVersion.url);
    }
    return {
      app_card: card[0] ?? null,
      en: mainELVersionMongo ?? null,
      vi: viCardDeckVersion ?? null,
    }
  }
  return card;
}
