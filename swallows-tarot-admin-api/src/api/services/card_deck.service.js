const CardDeck = require('../models/card_deck.model')
const Card = require('../models/card.model')
const Deck = require('../models/deck.model')
const mongoose = require('mongoose')
const { S3PATH, getFullUrl } = require('../../config/aws')
const { cardDeckStatus } = require('../../config/vars')

exports.getAll = async () => {
  const cardDecks = await CardDeck.aggregate().match({})
  const idCards = new Set()
  const idDecks = new Set()
  cardDecks.forEach((item) => {
    const { card_id, deck_id } = item
    idCards.add(card_id)
    idDecks.add(deck_id)
  })
  const [cards, decks] = await Promise.all([
    Card.find({
      _id: [...idCards],
    }),
    Deck.find({
      _id: [...idDecks],
    })
  ])
  const [cardsMap, decksMap] = await Promise.all([
    new Map(cards.map((item) => {
      const id = item._id.toString()
      return [id, item.name]
    })),
    new Map(decks.map((item) => {
      const id = item._id.toString()
      return [id, item.name]
    }))
  ])
  const result = cardDecks.map((item) => {
    const idCard = item.card_id.toString()
    const idDeck = item.deck_id.toString()
    const card = cardsMap.get(idCard)
    const deck = decksMap.get(idDeck)
    return { ...item, image_url: item.url != undefined ? (getFullUrl(S3PATH.card_deck, item.url)) : '', card, deck }
  })
  return result
}

exports.getCardDecksByPage = async ({ page = 1, order, field, filter, keyword, number = 10, suit }) => {
  let cardDecks
  number = parseInt(number);
  number = (isNaN(number) || number < 10) ? 10 : number;
  page = parseInt(page);
  page = (isNaN(page) || page <= 0) ? 1 : page;
  const query = filter ? { deck_id: filter } : {}
  query.language = 'en'
  query.isOriginal = true
  if (keyword) {
    query.name = new RegExp(keyword, 'gi')
  }
  const length = await CardDeck.count(query)

  if (order) {
    const type = order === "descend" ? -1 : 1
    const condition = {}
    condition[field] = type
    cardDecks = await CardDeck.find(query).populate('card_id', 'suit_id').sort(condition)
      .skip(number * (page - 1))
      .limit(number)
      .lean()
  } else {
    cardDecks = await CardDeck.find(query).populate('card_id', 'suit_id').sort({ updatedAt: -1 })
      .skip(number * (page - 1))
      .limit(number)
      .lean()
  }
  const idCards = new Set()
  const idDecks = new Set()
  const idGroups = new Set()
  const idCardDecks = new Set()
  let filterCardDecks = []
  cardDecks.forEach((item) => {
    if ((suit && item.card_id?.suit_id == suit) || (!suit || suit == 0)) {
      const { _id, card_id, deck_id, group_id } = item
      idCards.add(card_id)
      idDecks.add(deck_id)
      idGroups.add(group_id)
      idCardDecks.add(_id)
      filterCardDecks = [...filterCardDecks, item]
    }
  })
  const [cards, decks, cardDecksOtherLanguage] = await Promise.all([
    Card.find({
      _id: [...idCards],
    }),
    Deck.find({
      _id: [...idDecks],
    }),
    CardDeck.find()
      .in('group_id', [...idGroups])
      .nin('language', 'en')
      .where({ isOriginal: true })
  ])
  const [cardsMap, decksMap] = await Promise.all([
    new Map(cards.map((item) => {
      const id = item._id.toString()
      return [id, item.name]
    })),
    new Map(decks.map((item) => {
      const id = item._id.toString()
      return [id, item.name]
    }))
  ])
  const groupsMap = new Map()
  cardDecksOtherLanguage.forEach(item => {
    const idGroup = item.group_id.toString()
    const group = groupsMap.get(idGroup) || {}
    group[item.language] = item._id
    groupsMap.set(idGroup, group)
  })
  // const result = cardDecks.map((item) => {
  const result = filterCardDecks.map((item) => {
    const idCard = item.card_id.toString()
    const idDeck = item.deck_id.toString()
    const idGroup = item.group_id.toString()
    const card = cardsMap.get(idCard)
    const deck = decksMap.get(idDeck)
    const group = groupsMap.get(idGroup) || {}
    group[item.language] = item._id
    return { ...item, card, deck, image_url: item.url != undefined ? (getFullUrl(S3PATH.card_deck, item.url)) : '', group }
  })
  return {
    sizeDocuments: length,
    data: result,
    currentPage: page
  }
}

exports.getOneById = async (_id) => {
  const cardDeck = await CardDeck.findById(_id).lean()
  const [card, deck] = await Promise.all([
    Card.findById(cardDeck?.card_id).lean(),
    Deck.findById(cardDeck?.deck_id).lean()
  ])
  const group = await CardDeck.find({ _id: { "$ne": cardDeck._id }, 'group_id': cardDeck.group_id, 'language': cardDeck.language })
  return {
    ...cardDeck,
    image_url: cardDeck.url != undefined ? (getFullUrl(S3PATH.card_deck, cardDeck.url)) : '',
    card: card?.name || '',
    deck: deck?.name || '',
    group: group.filter(item => ({ id: item._id, created_by: item.created_by }))
  }
}

exports.getOneByName = async (name) => {
  return await CardDeck.findOne({ name })
}

exports.create = async (info) => {
  info.card_id = mongoose.Types.ObjectId(info.card_id)
  info.deck_id = mongoose.Types.ObjectId(info.deck_id)
  const cardDeck = await CardDeck(info)
  return await cardDeck.save()

}

exports.update = async (id, info) => {
  info.card_id = mongoose.Types.ObjectId(info.card_id)
  info.deck_id = mongoose.Types.ObjectId(info.deck_id)
  return await CardDeck.findByIdAndUpdate(id, { ...info }, { new: true })
}

exports.updateMany = async (condition, params) => {
  return await CardDeck.updateMany(condition, params)
}

exports.deleteOne = async (id) => {
  return await CardDeck.findByIdAndDelete(id)
}

exports.approve = async (ids) => {
  return await CardDeck.find({ _id: ids })
    .update({}, { state: cardDeckStatus.approved })
}

exports.getOneByNameExceptId = async ({ name, id }) => {
  return await CardDeck.findOne({ name, _id: { "$ne": id } })
}

exports.getCardDeckVersionsById = async (_id) => {
  // Todo fix later
  const card = await CardDeck.findOne({ _id }).populate('deck_id').lean()
  if (!card) {
    return [];
  }
  const [dumpV1, dumpV2] = await CardDeck.find({ group_id: card.group_id }).lean()
  [card, dumpV1, dumpV2].map((e) => {
    if (e.url) {
      e.full_url = getFullUrl('card_deck/', e.url);
    }
  })
  const result = {
    card,
    versions: [
      dumpV1,
      dumpV2
    ]
  }
  return result;
}
