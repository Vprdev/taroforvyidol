const Message = require('../models/message.model')
const UserSQL = require('../modelTaro/user.model');
const { roles } = require('../../config/vars');
const jwt = require('jsonwebtoken');

const Quote = require('inspirational-quotes');
const { forEach } = require('lodash');


const handleSocket = (io, socket) => {

  socket.broadcast.emit("user connected", {
    userID: socket.userID,
    username: socket.username,
    connected: true,
  });

  socket.join(socket.userID);
  socket.emit("socket userID", {
    userID: socket.userID,
    username: socket.username
  });

  const users= [];
  for(let [id, socket] of io.of("/").sockets) {
    users.push( {
      userID: socket.userID,
      username: socket.username,
      connected: socket.connected,
    });
  }
  socket.emit("users", users);

  socket.on("typing", (data)=> {
    socket.broadcast.emit("typing", data);
  })

  socket.on("private message",async ({content, to}) => {
    const message = {
      content,
      from: socket.userID,
      to,
    };
    // const froms= socket.userID;
    // console.log("message: ", message);
    // const response = await storeMessage({message: content, room: to, userID: froms});
    socket.to(to).to(socket.userID).emit("private message", message);
  })

  // notify users upon disconnection
  socket.on("disconnect", async () => {
    const matchingSockets = await io.in(socket.userID).allSockets();
    const isDisconnected = matchingSockets.size === 0;
    if (isDisconnected) {
      // notify other users
      socket.broadcast.emit("user disconnected", socket.userID);
    }
  });
}

const getuserID = (token) => {
  const decode = jwt.verify(token, process.env.JWT_SECRET);
  const userID = decode.sub;
  return userID;
}
const getUserName = async function(userID) {
  const data = await UserSQL.findUser(userID);
  return data[0].name;
}

const storeMessage = async function ({ message, room, userID }) {
  const data = await Message({ message, room, user_id: userID })
  return await data.save()
};

const getListMessage = async ({ page = 1, per_page = 20, room }) => {
  page = parseInt(page);
  per_page = parseInt(per_page);
  per_page = (isNaN(per_page) || page <= 0) ? 20 : per_page;
  const sizeDocuments = await Message.count({ room })
  const listMessages = await Message.find({ room })
    .sort({ createdAt: -1 })
    .skip((page - 1) * per_page)
    .limit(per_page)
    .lean()
  //Order list newest in last
  return {
    sizeDocuments,
    data: listMessages.reverse(),
    currentPage: page
  }
};

const getChatBotRoomId = (userID) => {
  return `bot-room-user-${userID}`
}

const getListRoom = async (req) => {
  let conditions = [];
  conditions = [
    ...conditions,
    {
      field: 'users.taro_role',
      operator: '<>',
      binding: roles.ADMIN,
      type: 'OR'
    }
  ]
  if (req.query.keywords) {
    const keywords = req.query.keywords;
    conditions = [
      ...conditions,
      {
        operator: 'RAW',
        binding: `(users.name LIKE '%${keywords}%' OR users.first_name LIKE '%${keywords}%' OR users.last_name LIKE '%${keywords}%' OR users.email LIKE '%${keywords}%')`,
        type: 'AND'
      }
    ]
  }

  const params = {
    ...req.query,
    conditions,
    columns: [
      'users.id as user_id',
      'users.name',
      'users.first_name',
      'users.last_name',
      'users.email',
      'users.taro_role',
      `CONCAT('bot-room-user-',users.id) as room`
    ],
  }

  const [users, total] = await Promise.all([
    UserSQL.listByConditions(params),
    UserSQL.getCountTotal(params)
  ])
  return {
    data: users,
    total: total[0]?.total ?? 0
  };
}

module.exports = {
  handleSocket,
  storeMessage,
  getListMessage,
  getChatBotRoomId,
  getListRoom,
  getUserName,
  getuserID
};