const Card = require('../models/card.model');
const dbMySQL = require('./../../config/mysql.js');

exports.searchCards = async (name) => {
  if (name) {
    const regex = new RegExp(name, 'gi');
    return await Card.find({ name: regex })
  }
  return await Card.find({})
}

exports.getOneById = async (id) => {
  return await Card.findById(id)
}

exports.getAllCardsFromMySQL = () => {
  const cards = new Promise((res, rej) => {
    dbMySQL.query('SELECT * FROM cards', (err, result) => {
      if (!err) res(result);
      else rej(err);
    });
  });
  return cards.then(result => result)
    .catch(error => error);
}