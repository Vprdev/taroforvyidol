const axios = require('axios')
const { getToAppApi, postToAppApi, putToAppApi, removeToAppApi } = require('../helpers/call_api')
const moment = require('moment-timezone');

exports.getListById = async (cardDeckId, {page = 1, number = 10, user_id}) => {
  const params = {
    per_page: number,
    page,
    card_deck_id: cardDeckId,
    user_id
  }
  return await getToAppApi('comment', params)
}

exports.createComment = async (cardDeckId, body) => {
  const params = {
    card_deck_id: cardDeckId,
    content: {
      msg: body.msg,
      created_at: moment().utc().format()
    }
  }
  const query = {
    user_id: body.user_id
  }
  return await postToAppApi('comment', params, query)
}

exports.editComment = async (commentId, body) => {
  const params = {
    content: {
      msg       : body.msg,
      created_at: body.created_at
    }
  }
  const query = {
    user_id: body.user_id
  }
  return await putToAppApi(`comment/${commentId}`, params, query)
}

exports.deleteComment = async (commentId, query) => {
  return await removeToAppApi(`comment/${commentId}`, query)
}
