const Deck = require("../models/deck.model");
const { S3PATH, getFullUrl } = require("../../config/aws");

exports.getAll = async (name) => {
  if (name) {
    const regex = new RegExp(name, "gi");
    return await Deck.find({ name: regex }).sort({
      name: 1,
    });
  }
  return await Deck.find({}).sort({ name: 1 });
};

exports.getDecksByPage = async ({
  page = 1,
  order,
  field,
  keyword,
  number = 10,
}) => {
  let result;
  number = parseInt(number);
  number = isNaN(number) || number < 10 ? 10 : number;
  page = parseInt(page);
  page = isNaN(page) || page <= 0 ? 1 : page;
  const query = keyword ? { name: new RegExp(keyword, "gi") } : {};
  const length = await Deck.count(query);
  if (order) {
    const type = order === "descend" ? -1 : 1;
    const condition = {};
    condition[field] = type;
    result = await Deck.find(query)
      .sort(condition)
      .skip(number * (page - 1))
      .limit(number)
      .lean();
  } else {
    result = await Deck.find(query)
      .sort({ createdAt: -1 })
      .skip(number * (page - 1))
      .limit(number)
      .lean();
  }
  return {
    sizeDocuments: length,
    data: result.map((item) => ({
      ...item,
      image_url:
        item.url != undefined ? getFullUrl(S3PATH.card_deck, item.url) : "",
    })),
    currentPage: page,
  };
};

exports.getOneById = async (id) => {
  return await Deck.findById(id);
};

exports.getOneByName = async (name) => {
  return await Deck.findOne({ name });
};

exports.isExisted = async (id, name) => {
  return await Deck.findOne({ name, _id: { $ne: id } });
};

exports.create = async (info) => {
  const created = new Deck(info);
  return await created.save();
};

exports.update = async (id, info) => {
  return await Deck.findByIdAndUpdate(id, { ...info }, { new: true });
};

exports.deleteOne = async (id) => {
  return await Deck.findByIdAndDelete(id);
};

exports.getOneByNameExceptId = async ({ name, id }) => {
  return await CardDeck.findOne({ name, _id: { $ne: id } });
};
