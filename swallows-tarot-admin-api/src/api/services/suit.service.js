const Suit = require('../models/suit.model');
const dbMySQL = require('./../../config/mysql.js');

exports.getAll = async () => {
  return await Suit.find({})
}

exports.getOneById = async (id) => {
  return await Suit.findById(id)
}

exports.create = async (info) => {
  const created = new Suit(info)
  return created.save()
}


exports.getAllSuitsFromMySQL = async () => {
  const suits = new Promise((res, rej) => {
    dbMySQL.query('SELECT * FROM suits', (err, result) => {
      if (!err) res(result);
      else rej(err);
    });
  });
  return suits.then(result => result)
    .catch(error => error);
}