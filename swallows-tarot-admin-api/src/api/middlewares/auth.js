const passport = require('passport');
const response = require('../helpers/apiResponse')
const { ADMIN, USER } = require('../../config/vars').roles

const handleJWT = (req, res, next, roles) => async (err, user, info) => {
  const error = err || info;
  if (error || !user) {
    return response.customReponseNoData(res, 401, 'Unauthorized')
  }
  if (
    !roles.includes(user.taro_role)
    && !roles.includes(USER)
    && user.taro_role !== ADMIN
  ) {
    return response.customReponseNoData(res, 403, 'Forbidden')
  }
  req.user = user;
  return next();
};

exports.authorize = (roles) => (req, res, next) =>
  passport.authenticate(
    'jwt', { session: false },
    handleJWT(req, res, next, roles),
  )(req, res, next);

exports.oAuth = service =>
  passport.authenticate(service, { session: false });
