const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
  user_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  room: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  deleted_at: { type: Date },
}, {
  timestamps: true,
});
const Message = mongoose.model('Message', messageSchema);
module.exports = Message;
