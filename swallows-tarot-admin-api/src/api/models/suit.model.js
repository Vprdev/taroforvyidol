const mongoose = require('mongoose');
const dbMySQL = require('./../../config/mysql.js');

const suitSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  deleted_at: { type: Date },
}, {
  timestamps: true,
});
const Suit = mongoose.model('Suit', suitSchema);
module.exports = Suit;
