const mongoose = require('mongoose');
const { cardDeckStatus, language, userDefault } = require('../../config/vars')

const cardDeckSchema = new mongoose.Schema({
  group_id: {
    type: mongoose.Schema.Types.ObjectId,
    auto: true
  },
  name: {
    type: String,
    required: true,
  },
  card_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Card'
  },
  deck_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Deck',
    required: true,
  },
  quote: {
    type: String,
  },
  meaning: {
    type: Array,
  },
  reference_url: {
    type: String,
  },
  url: {
    type: String,
  },
  keyword: {
    type: Array,
  },
  state: {
    type: String,
    default: cardDeckStatus.processing
  },
  language: {
    type: String,
    default: language.en
  },
  created_by: {
    type: Object,
    default: userDefault
  },
  isOriginal: {
    type: Boolean,
    default: true
  },
  deleted_at: { type: Date },
}, {
  timestamps: true,
});
const CardDeck = mongoose.model('CardDeck', cardDeckSchema);
module.exports = CardDeck;
