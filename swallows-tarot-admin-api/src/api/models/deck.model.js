const mongoose = require('mongoose');

const deckSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  source: {
    type: Array,
  },
  author: {
    type: String,
  },
  deleted_at: { type: Date },
}, {
  timestamps: true,
});
const Deck = mongoose.model('Deck', deckSchema);
module.exports = Deck;
