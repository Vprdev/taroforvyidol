const mongoose = require('mongoose');
const dbMySQL = require('./../../config/mysql.js');

const cardSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  content: {
    type: String,
  },
  suit_id: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  fortune_telling: {
    type: Array,
  },
  keywords: {
    type: Array,
  },
  number: {
    type: String,
  },
  meanings_light: {
    type: Array,
  },
  meanings_shadow: {
    type: Array,
  },
  archetype: {
    type: String,
  },
  hebrew_alphabet: {
    type: String,
  },
  numerology: {
    type: String,
  },
  elemental: {
    type: String,
  },
  mythical_spiritual: {
    type: String,
  },
  astrology: {
    type: String,
  },
  affirmation: {
    type: String,
  },
  questions_to_ask: {
    type: Array,
  },
  deleted_at: { type: Date },
}, {
  timestamps: true,
});
const Card = mongoose.model('Card', cardSchema);
module.exports = Card;
