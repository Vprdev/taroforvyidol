const express = require('express');
const controller = require('../../controllers/chat_bot.controller');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/messages')
  /**
   * @api {get} /v1/chat-bot/messages?room=bot-room-user-1&page=1&per_page=3 List messages
   * @apiDescription Get list messagges in chatbot
   * @apiVersion 1.0.0
   * @apiName List messages
   * @apiParam {String} room=required Room Chat
   * @apiParam {Number} [page] current page
   * @apiParam {Number} [per_page] per_page
   * @apiGroup ChatBot
   *
   * @apiSuccessExample Response
   * HTTP/1.1 200 OK
   *  {
   *      "message": "SUCCESS",
   *      "error": null,
   *      "data": {
   *          "sizeDocuments": 5,
   *          "data": [
   *              {
   *                  "_id": "5ff6b9396ef3792a86eaa8a9",
   *                  "message": "232",
   *                  "room": "bot-room-user-1",
   *                  "user_id": 1,
   *                  "createdAt": "2021-01-07T07:33:13.736Z",
   *                  "updatedAt": "2021-01-07T07:33:13.736Z",
   *                  "__v": 0
   *              },
   *              {
   *                  "_id": "5ff6b93f6ef3792a86eaa8ab",
   *                  "message": "2323",
   *                  "room": "bot-room-user-1",
   *                  "user_id": 1,
   *                  "createdAt": "2021-01-07T07:33:19.226Z",
   *                  "updatedAt": "2021-01-07T07:33:19.226Z",
   *                  "__v": 0
   *              },
   *              {
   *                  "_id": "5ff6b9416ef3792a86eaa8ac",
   *                  "message": "asdasd",
   *                  "room": "bot-room-user-1",
   *                  "user_id": 2,
   *                  "createdAt": "2021-01-07T07:33:21.971Z",
   *                  "updatedAt": "2021-01-07T07:33:21.971Z",
   *                  "__v": 0
   *              }
   *          ],
   *          "currentPage": 1
   *      }
   *  }
   */
  .get(controller.getListMessage)
  // .get(authorize([roles.USER, roles.READER, roles.ADMIN]), controller.getListMessage)

  router
  .route('/rooms')
  .get( controller.getListRoom)
  // .get(authorize([roles.ADMIN]), controller.getListRoom)
  /**
   * @api {get} /v1/chat-bot/rooms?number=20&page=1&keywords=a List Rooms Chatbot
   * @apiDescription Get list rooms in chatbot
   * @apiVersion 1.0.0
   * @apiName List Rooms
   * @apiParam {Number} [page] current page
   * @apiParam {Number} [number] per_page
   * @apiParam {String} [keywords] keywords Filter by name, first name, last name or email
   * @apiGroup ChatBot
   *
   * @apiSuccessExample Response
   * HTTP/1.1 200 OK
   *   {
   *       "message": "SUCCESS",
   *       "error": null,
   *       "data": {
   *           "data": [
   *               {
   *                   "user_id": 2,
   *                   "name": "super 002",
   *                   "first_name": "Test",
   *                   "last_name": "Name",
   *                   "email": "test002@email.com",
   *                   "taro_role": 2,
   *                   "room": "bot-room-user-2"
   *               },
   *               ...
   *               {
   *                   "user_id": 22,
   *                   "name": null,
   *                   "first_name": "Top",
   *                   "last_name": "Top",
   *                   "email": "top@gmail.com",
   *                   "taro_role": 1,
   *                   "room": "bot-room-user-22"
   *               }
   *           ],
   *           "total": 31
   *       }
   *   }
   */
module.exports = router;
