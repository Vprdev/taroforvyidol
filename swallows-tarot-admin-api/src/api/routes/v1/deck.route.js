const express = require('express');
const controller = require('../../controllers/deck.controller');
const validate = require('express-validation');
const { authorize } = require('../../middlewares/auth');
const {
  createDeck,
  updateDeck,
} = require('../../validations/deck.validation');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/')
  /**
   * @api {get} v1/decks List Decks
   * @apiDescription Get a list of decks
   * @apiVersion 1.0.0
   * @apiName ListDecks
   * @apiGroup Deck
   *
   * @apiSuccess {Object[]} decks List of decks.
   */
  .get(controller.getAllDecks)
  /**
   * @api {post} v1/decks Create Deck
   * @apiDescription Create a new deck
   * @apiVersion 1.0.0
   * @apiName CreateDeck
   * @apiGroup Deck
   *
   * @apiParam  {String}             name               Deck's name
   * @apiParam  {String}             [description]      Deck's description
   * @apiParam  {Array}              [source]           Deck's source
   * @apiParam  {String}             [author]           Deck's author
   *
   * @apiSuccess (Created 201) {String}  _id            Deck's id
   * @apiSuccess (Created 201) {String}  name           Deck's name
   * @apiSuccess (Created 201) {String}  description    Deck's description
   * @apiSuccess (Created 201) {String}  source         Deck's role
   * @apiSuccess (Created 201) {String}  author         Deck's role
   * @apiSuccess (Created 201) {Date}    createdAt      Timestamp
   * @apiSuccess (Created 201) {Date}    updatedAt      Timestamp
   *
   * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
   */
  .post(authorize([roles.READER]), validate(createDeck), controller.createDeck);

router
  .route('/pagination')
  .get(controller.getDecksByPage)

router
  .route('/:deckId')
  /**
   * @api {get} v1/decks/:id Get Deck
   * @apiDescription Get deck information
   * @apiVersion 1.0.0
   * @apiName GetDeck
   * @apiGroup Deck
   *
   * @apiSuccess {String}     _id              Deck's id
   * @apiSuccess {String}     name             Deck's name
   * @apiSuccess {String}     description      Deck's description
   * @apiSuccess {Array}      source           Deck's source
   * @apiSuccess {String}     author           Deck's author
   * @apiSuccess {Date}       createdAt        Timestamp
   * @apiSuccess {Date}       updatedAt        Timestamp
   *
   * @apiError (Not Found 404)    NotFound     Deck does not exist
   */
  .get(authorize([roles.USER]), controller.get)
  /**
   * @api {patch} v1/decks/:id Update Deck
   * @apiDescription Update some fields of a deck document
   * @apiVersion 1.0.0
   * @apiName UpdateDeck
   * @apiGroup Deck
   *
   * @apiParam  {String}             [name]             Deck's name
   * @apiParam  {String}             [description]      Deck's description
   * @apiParam  {Array}              [source]           Deck's source
   * @apiParam  {String}             [author]           Deck's author
   *
   * @apiSuccess {String}     _id              Deck's id
   * @apiSuccess {String}     name             Deck's name
   * @apiSuccess {String}     description      Deck's description
   * @apiSuccess {Array}      source           Deck's source
   * @apiSuccess {String}     author           Deck's author
   * @apiSuccess {Date}       createdAt        Timestamp
   * @apiSuccess {Date}       updatedAt        Timestamp
   *
   * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
   * @apiError (Not Found 404)    NotFound     deck does not exist
   */
  .put(authorize([roles.READER]), validate(updateDeck), controller.updateDeck)
  /**
   * @api {patch} v1/decks/:id Delete Deck
   * @apiDescription Delete a deck
   * @apiVersion 1.0.0
   * @apiName DeleteDeck
   * @apiGroup Deck
   *
   * @apiSuccess (No Content 204)  Successfully deleted
   *
   * @apiError (Not Found 404)    NotFound      Deck does not exist
   */
  .delete(authorize([roles.ADMIN]), controller.delete);
module.exports = router;
