const express = require('express');
const controller = require('../../controllers/card.controller');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/')
  /**
   * @api {get} v1/cards List Cards
   * @apiDescription Get a list of cards
   * @apiVersion 1.0.0
   * @apiName ListCards
   * @apiGroup Card
   *
   * @apiSuccess {Object[]} cards List of cards.
   */
  .get(controller.getAllCards);

module.exports = router;
