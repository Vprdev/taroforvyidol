const express = require('express');
const controller = require('../../controllers/feedback.controller');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/')
  /**
   * @api {get} v1/feedbacks List Feedbacks
   * @apiDescription Get a list of feedbacks
   * @apiVersion 1.0.0
   * @apiName ListFeedbacks
   * @apiGroup Suit
   *
   * @apiSuccess {Object[]} feedbacks List of feedbacks.
   */
  .get(authorize([roles.USER]), controller.getAllFeedbacks)
module.exports = router;
