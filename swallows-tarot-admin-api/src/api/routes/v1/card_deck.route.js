const express = require('express');
const controller = require('../../controllers/card_deck.controller');
const validate = require('express-validation');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/')
  /**
   * @api {get} v1/cardDecks List Card Deck
   * @apiDescription Get a list of card deck
   * @apiVersion 1.0.0
   * @apiName ListCardDecks
   * @apiGroup Card Deck
   *
   * @apiSuccess {Object[]} cardDecks List of card deck.
   */
  .get(authorize([roles.USER]), controller.getAllCardDeck)
  /**
   * @api {post} v1/cardDecks Create Card Deck
   * @apiDescription Create a new card deck
   * @apiVersion 1.0.0
   * @apiName CreateCardDeck
   * @apiGroup Card Deck
   *
   * @apiParam  {String}             name               Card Deck's name
   * @apiParam  {String}             card_id            Card's id
   * @apiParam  {String}             deck_id            Deck's id
   * @apiParam  {Array}              [contents]         Card Deck's contents
   * @apiParam  {String}             [detail]           Card Deck's detail
   * @apiParam  {File}               [url]              Card Deck's image required when language is EL
   * @apiParam  {String}             [language]         Card Deck's language (el, vi)
   * @apiParam  {String}             [group_id]         Card Deck's group_id (el: auto, vi: el group_id card)
   *
   * @apiSuccess (Created 201) {String}  _id            Card Deck's id
   * @apiSuccess (Created 201) {String}  name           Card Deck's name
   * @apiSuccess (Created 201) {String}  deck_id        Deck's id
   * @apiSuccess (Created 201) {String}  deck           Deck's name
   * @apiSuccess (Created 201) {String}  card_id        Card's id
   * @apiSuccess (Created 201) {String}  card           Card's name
   * @apiSuccess (Created 201) {Array}   contents       Card Deck's contents
   * @apiSuccess (Created 201) {String}  detail         Card Deck's detail
   * @apiSuccess (Created 201) {String}  image_url      Card Deck's image
   * @apiSuccess (Created 201) {Date}    createdAt      Timestamp
   * @apiSuccess (Created 201) {Date}    updatedAt      Timestamp
   *
   * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
   */
  .post(authorize([roles.READER]), controller.createCardDeck)

/**
 * @api {get} v1/cardDecks/pagination List Card Deck Pagination
 * @apiSampleRequest v1/cardDecks/pagination?language=vi&page=1&filter=5f0dc1f3f5ab44239f17083e&field=_id&order=descend
 * @apiDescription Get a list of card deck
 * @apiVersion 1.0.0
 * @apiName ListCardDecks Pagination
 * @apiGroup Card Deck
 *
 * @apiParam  {String} [page]             Current page
 * @apiParam  {String} [filter]           Filter by deck_id
 * @apiParam  {String} [field]            Field order
 * @apiParam  {String} [order]            Order 'descend'|'ascend' by fileld
 * @apiParam  {String} [keyword]          Search by Card Deck's name
 * @apiParam  {String} [suit]             filter by Suit
 *
 * @apiSuccess {Object[]} cardDecks List of card deck.
 */
router
  .route('/pagination')
  .get(controller.getCardDecksByPage)

/**
 * @api {get} v1/cardVersions/:cardDeckId Get Card Deck and List verions
 * @apiSampleRequest v1/cardVersions/12345
 * @apiDescription Get Card Deck and List verions
 * @apiVersion 1.0.0
 * @apiName Card Deck and version
 * @apiGroup Card Deck
 *
 */
router
  .route('/cardVersions/:cardDeckId')
  .get(controller.getCardVersions)

router
  .route('/approve')
  /**
   * @api {patch} v1/cardDecks/approve Approve Card Decks
   * @apiDescription Update field state of a card deck document
   * @apiVersion 1.0.0
   * @apiName ApproveCardDecks
   * @apiGroup Card Deck
   *
   * @apiParam  {Array}              [ids]         List id of card decks
   *
   * @apiSuccess (No Content 204)       Seccuessfully updated
   *
   * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
   * @apiError (Not Found 404)    NotFound         Card Deck does not exist
   */
  .put(authorize([roles.ADMIN]), controller.approve)

router
  .route('/:cardDeckId')
  /**
   * @api {get} v1/cardDecks/:id Get Card Deck
   * @apiDescription Get card deck information
   * @apiVersion 1.0.0
   * @apiName GetCardDeck
   * @apiGroup Card Deck
   *
   * @apiSuccess {String}     _id              Card Deck's id
   * @apiSuccess {String}     name             Card Deck's name
   * @apiSuccess {String}     deck_id          Deck's id
   * @apiSuccess {String}     deck             Deck's name
   * @apiSuccess {String}     card_id          Card's id
   * @apiSuccess {String}     card             Card's name
   * @apiSuccess {String}     detail           Card Deck's detail
   * @apiSuccess {String}     image_url        Card Deck's image url
   * @apiSuccess {Array}      contents         Card Deck's contents
   * @apiSuccess {Date}       createdAt        Timestamp
   * @apiSuccess {Date}       updatedAt        Timestamp
   *
   * @apiError (Not Found 404)    NotFound     Card Deck does not exist
   */
  .get(controller.get)
  /**
   * @api {patch} v1/cardDecks/:id Update Card Deck
   * @apiDescription Update some fields of a card deck document
   * @apiVersion 1.0.0
   * @apiName UpdateCardDeck
   * @apiGroup Card Deck
   *
   * @apiParam  {String}             [name]             Card Deck's name
   * @apiParam  {String}             [deck_id]          Deck's id
   * @apiParam  {String}             [detail]           Card Deck's detail
   * @apiParam  {Array}              [contents]         Card Deck's contents
   * @apiParam  {String}             [url]              Card Deck's url
   * @apiParam  {String}             group_id           Card Deck's group id
   * @apiParam  {String}             language           Card Deck's language
   *
   * @apiSuccess {String}     _id              Card Deck's id
   * @apiSuccess {String}     name             Card Deck's name
   * @apiSuccess {String}     deck_id          Deck's id
   * @apiSuccess {String}     card_id          Card's id
   * @apiSuccess {String}     detail           Card Deck's detail
   * @apiSuccess {Array}      contents         Card Deck's contents
   * @apiSuccess {Date}       createdAt        Timestamp
   * @apiSuccess {Date}       updatedAt        Timestamp
   *
   * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
   * @apiError (Not Found 404)    NotFound         Card Deck does not exist
   */
  .put(authorize([roles.READER]), controller.updateCardDeck)
  /**
   * @api {patch} v1/cardDecks/:id Delete Card Deck
   * @apiDescription Delete a card deck
   * @apiVersion 1.0.0
   * @apiName DeleteCardDeck
   * @apiGroup Card Deck
   *
   * @apiSuccess (No Content 204)  Successfully deleted
   *
   * @apiError (Not Found 404)    NotFound      Card Deck does not exist
   */
  .delete(authorize([roles.ADMIN]), controller.delete);

module.exports = router;
