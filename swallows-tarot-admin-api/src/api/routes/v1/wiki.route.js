const express = require('express');
const controller = require('../../controllers/wiki.controller');
const commentController = require('../../controllers/comment.controller');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars');
const validate = require('express-validation');
const { createComment, editComment } = require('../../validations/comment.validation')

const router = express.Router();

router
  .route('/card-decks')
  /**
   *
   * @api {get} /v1/wiki/card-decks?number=2&page=2&deck=rider-waite&suit=swords get Wiki list Card deck...
   * @apiDescription Get Wiki list Card deck
   * @apiVersion 1.0.0
   * @apiName ListWikiCardDecks
   * @apiParam {Number} [page] Current Page
   * @apiParam {Number} [number] per_page
   * @apiParam {String=major|wands|cups|swords|pentacles} [suit] Filter by suit major:1|wands:2|cups:3|4:swords|5:pentacles
   * @apiParam {String} [keyword] search by card name
   * @apiParam {Number} [deck] filer by deck slug
   * @apiGroup Wiki
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 OK
   *  {
   *      "message": "SUCCESS",
   *      "error": null,
   *      "data": {
   *          "data": [
   *              {
   *                  "id": 10940,
   *                  "name": "Three of Swords",
   *                  "deck_id": 145,
   *                  "slug": "three-of-swords-145",
   *                  "meaning": [
   *                      "General Meaning",
   *                      "Traditionally, the Three of Swords signified separation or the breakup of a significant relationship, including the tragic emotions that come along with such an event. Some cards show the horizon filled with storm clouds and flashing lightning.",
   *                      "The positive side of this card would be the ending of draining or frustrating associations, to become free again. One message this card brings is to cut some things loose. In spite of sentimental memories and emotional attachments, we need to let go of relationships that either cannot live up to their promise or have outlived the pleasure and support we once found in them.",
   *                      ""
   *                  ],
   *                  "keywords": [],
   *                  "created_at": "2020-12-17T07:37:09.000Z",
   *                  "card_id": 53,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Rider_Waite/Three_of_Swords.jpg",
   *                  "suit_id": 4,
   *                  "deck_name": "Rider Waite"
   *              },
   *              {
   *                  "id": 10941,
   *                  "name": "Four of Swords",
   *                  "deck_id": 145,
   *                  "slug": "four-of-swords-145",
   *                  "meaning": [
   *                      "General Meaning",
   *                      "A Four in this suit sends a message to take some time out, surrendering worldly concerns and retreating to a sheltered place of serenity away from the hustle and bustle. The oldest Tarot card images suggest a visit to the tombs of our ancestors, a place to contemplate your mortality and breathe in the dust of those who brought you here.",
   *                      "A vision quest or pilgrimage to one's own center allows us to contemplate our roots, values and goals. Here you will see your place in the flow of time ... and unfolding generations.",
   *                      ""
   *                  ],
   *                  "keywords": [],
   *                  "created_at": "2020-12-17T07:37:09.000Z",
   *                  "card_id": 54,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Rider_Waite/Four_of_Swords.jpg",
   *                  "suit_id": 4,
   *                  "deck_name": "Rider Waite"
   *              }
   *          ],
   *          "total": 14
   *      }
   *  }
   */
  .get(controller.getCardDecks)

router
  .route('/decks')
  /**
   * @api {get} /v1/wiki/decks?number=4&page=1 get Wiki list  deck
   * @apiDescription Get Wiki list  deck
   * @apiVersion 1.0.0
   * @apiName ListWikiDecks
   * @apiParam {Number} [page] Current Page
   * @apiParam {Number} [number] per_page
   * @apiParam {String} [keyword] search by deck name
   * @apiGroup Wiki
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 OK
   *  {
   *      "message": "SUCCESS",
   *      "error": null,
   *      "data": {
   *          "data": [
   *              {
   *                  "id": 1,
   *                  "name": "Starter",
   *                  "slug": "starter",
   *                  "source": "https://www.tarot.com",
   *                  "description": "The Starter Tarot deck is perfect for beginners, with very simple artwork in the style of ancient woodcuts based on classic motifs, and both the regular and reverse meanings printed on each card.",
   *                  "created_at": "2020-12-17T07:37:10.000Z",
   *                  "updated_at": "2020-12-17T07:37:10.000Z",
   *                  "author": null,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Starter/starter.jpg"
   *              },
   *              {
   *                  "id": 2,
   *                  "name": "Old Path",
   *                  "slug": "old-path",
   *                  "source": "https://www.tarot.com",
   *                  "description": "Tarot of the Old Path was created by leaders of the Wiccan craft, and is aimed at Pagan Tarot readers. It features easy-to-understand images drawn from many diverse points of view.",
   *                  "created_at": "2020-12-17T07:37:10.000Z",
   *                  "updated_at": "2020-12-17T07:37:10.000Z",
   *                  "author": null,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Old_Path/old-path.jpg"
   *              },
   *              {
   *                  "id": 3,
   *                  "name": "Angel Answers Oracle Cards",
   *                  "slug": "angel-answers-oracle-cards",
   *                  "source": "Nguyễn Hiếu from https://www.tarot.vn",
   *                  "description": "",
   *                  "created_at": "2020-12-17T07:37:11.000Z",
   *                  "updated_at": "2020-12-17T07:37:11.000Z",
   *                  "author": null,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Angel_Answers_Oracle_Cards/Angel_Answers_Oracle_Cards.jpg"
   *              },
   *              {
   *                  "id": 4,
   *                  "name": "Silver Witchcraft Tarot",
   *                  "slug": "silver-witchcraft-tarot",
   *                  "source": "Magic Knight from https://www.tarot.vn",
   *                  "description": "Thể hiện trí tuệ và phép màu của từng thế hệ qua ánh trăng thanh tao của nữ thần Luna, bộ bài \nSilver Witchcraft Tarot\n \nkết hợp chuẩn tarot truyền thống Rider Waite với hệ thống biểu tượng Pagan đương đại, tạo nên một trải nghiệm giải bài mới mẻ và độc đáo. Được thêu dệt theo những chu kỳ của mặt trăng và các ngày lễ của Pagan giáo, bộ bài tarot đầy tinh tế này sẽ mang đến tri thức về bản chất vật chất, cảm xúc, và tinh thần, thầm thì những lời chỉ dẫn cho những ai muốn lắng nghe.\nXin vui lòng \nClick\n vào hình bên dưới để xem ý nghĩa từng lá bài.",
   *                  "created_at": "2020-12-17T07:37:11.000Z",
   *                  "updated_at": "2020-12-17T07:37:11.000Z",
   *                  "author": null,
   *                  "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Silver_Witchcraft_Tarot/Silver_Witchcraft_Tarot.jpg"
   *              }
   *          ],
   *          "total": 165
   *      }
   *  }
   */
  .get(controller.getDecks)

router.route('/card-deck/:id')
  /**
   * @api {get} /v1/wiki/card-deck/{card_slug} Get card Deck with version
   * @apiDescription Get card Deck with version
   * @apiVersion 1.0.0
   * @apiName GetWikiCardDeck
   * @apiGroup Wiki
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 OK
   *     {
   *      "message": "SUCCESS",
   *      "error": null,
   *      "data": {
   *            "app_card": {
   *                   "id": 10890,
   *                   "name": "The High Priestess",
   *                   "deck_id": 145,
   *                   "slug": "the-high-priestess-145",
   *                   "meaning": [
   *                       "General Meaning",
   *                       "The High Priestess is a Major Arcana, or trump card, that represents human wisdom. The High Priestess can be viewed as a kind of female Pope (a Papess), or the ancient Egyptian Priestess of Isis, the even more ancient snake and bird goddesses, the Greek goddess Persephone, or Eve, before the fall.",
   *                       "For the accused heretics who were burnt at the stake for revering her in the 14th and 15th century, the Priestess symbolized the prophecy of the return of the Holy Spirit, which was perceived as the female aspect of the Holy Trinity.",
   *                       "In terms of the Major Arcana ordering of cards, The High Priestess appears in the sequence as soon as the Fool decides he wants to develop his innate powers, making a move toward becoming a Magician. The High Priestess is his first teacher, representing the inner life and the method for contacting it, as well as the contemplative study of nature and spiritual mystery.",
   *                       ""
   *                   ],
   *                   "keywords": [],
   *                   "created_at": "2020-12-17T07:37:09.000Z",
   *                   "card_id": 3,
   *                   "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Rider_Waite/The_High_Priestess.jpg",
   *                   "suit_id": 1,
   *                   "deck_name": "Rider Waite"
   *               },
   *          "en": {
   *              "_id": "5fdc1adbe4b3215fb5a76345",
   *              "group_id": "5fdc1adbe4b3215fb5a76344",
   *              "meaning": [
   *                  "Bạn không nhất thiết phải làm tất cả mọi thứ một mình. Những Thiên Thần đang gợi ý cho bạn thử tìm kiếm cho bản thân một trợ lý từ những người xung quanh. Bằng cách cho phép người khác giúp đỡ bạn, bạn sẽ nâng cao những cơ hội đến với thành công của mình, cũng như những kế hoạch của bạn sẽ bắt đầu tiến lên phía trước. Sự giúp đỡ mà bạn nhận được cũng có thể bao gồm cả những thông tin quan trọng có được bởi vốn hiểu biết cá nhân.",
   *                  "Sự căng thẳng của việc ôm đồm quá nhiều thứ có thể sẽ tạo ra những thử thách về thể chất lẫn tinh thần cho bạn. Hãy ở bên cạnh bạn bè và gia đình của mình, những người luôn sẵn sàng giúp bạn làm nhẹ đi những gánh nặng, và từ đó bạn sẽ cảm thấy tốt hơn nhiều!"
   *              ],
   *              "keyword": [],
   *              "state": "PROCESSING",
   *              "language": "en",
   *              "created_by": {
   *                  "fullname": "super admin",
   *                  "user_id": 96
   *              },
   *              "isOriginal": true,
   *              "name": "Ask For Help From The Others",
   *              "index_count": 2,
   *              "url": "deck_folder/Angel_Answers_Oracle_Cards/Ask_For_Help_From_The_Others.png",
   *              "deck_id": "5fdc18baefd4b65ec22f811f",
   *              "card_id": "5fdc18baefd4b65ec22f811c",
   *              "createdAt": "2020-12-18T02:58:35.287Z",
   *              "updatedAt": "2020-12-18T02:58:35.287Z",
   *              "__v": 0,
   *              "full_url": "https://taro-app-admin-v2.s3.ap-southeast-1.amazonaws.com/card_deck/deck_folder/Angel_Answers_Oracle_Cards/Ask_For_Help_From_The_Others.png"
   *          },
   *          "vi": null
   *      }
   *  }
   */
  .get(controller.cardVersion)

router
  .route('/search-decks')
  /**
   * @api {get} /v1/wiki/search-decks?keywords=Waite Wiki search deck
   * @apiDescription Wiki search deck
   * @apiVersion 1.0.0
   * @apiName ListWikiDecks
   * @apiParam {Number} [page] Current Page
   * @apiParam {Number} [number] per_page
   * @apiParam {String} [keyword] search by deck name
   * @apiGroup Wiki
   *
   * @apiSuccessExample {json} Success-Response:
   * HTTP/1.1 200 OK
   *    {
   *     "message": "SUCCESS",
   *      "error": null,
   *      "data": [
   *          {
   *              "id": 34,
   *              "name": "8-Bit Tarot",
   *              "slug": "8-bit-tarot"
   *          },
   *          {
   *              "id": 140,
   *              "name": "African Tarot",
   *              "slug": "african-tarot"
   *          },
   *          {
   *              "id": 47,
   *              "name": "After Tarot",
   *              "slug": "after-tarot"
   *          }
   *      ]
   *    }
   */
  .get(controller.searchDecks)

router.route('/card-deck/:id/comments')
  .get(commentController.getList)
/**
* @api {get} /v1/wiki/card-deck/:id/comments?page=1&number=5 Get list comment by cardId
* @apiDescription Get List Comment Card Deck
* @apiVersion 1.0.0
* @apiName ListCommentCardDeck
* @apiParam {Number} [page] Current Page
* @apiParam {Number} [number] per_page
* @apiGroup Wiki
*
* @apiSuccessExample {json} Success-Response:
* HTTP/1.1 200 OK
*  {
*      "message": "SUCCESS",
*      "error": null,
*      "data": {
*          "status": true,
*          "data": {
*              "data": [
*                  {
*                      "id": 1,
*                      "content": {
*                          "Date": "09 April 2020",
*                          "Question": "Comment Question"
*                      },
*                      "user_id": 1,
*                      "card_deck_id": 50,
*                      "user_name": "super 001",
*                      "user_avatar": {
*                          "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
*                          "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
*                      },
*                      "reactions": {
*                          "list_reactions": {
*                              "type_1": 1,
*                              "type_2": 1,
*                              "type_3": 0,
*                              "type_4": 1,
*                              "type_5": 0,
*                              "type_6": 0,
*                              "type_7": 0
*                          },
*                          "pre_show_ids": [
*                              1,
*                              2,
*                              4
*                          ],
*                          "total_reactions": 3,
*                          "current_user_reactions": [
*                              1,
*                              2
*                          ]
*                      },
*                      "created_at": "2020-11-01 16:37:04"
*                  },
*                  {
*                      "id": 2,
*                      "content": {
*                          "Date": "25 April 2020",
*                          "Note": "He feel...",
*                          "Question": "Hello World"
*                      },
*                      "user_id": 1,
*                      "card_deck_id": 50,
*                      "user_name": "super 001",
*                      "user_avatar": {
*                          "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
*                          "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
*                      },
*                      "reactions": {
*                          "list_reactions": {
*                              "type_1": 1,
*                              "type_2": 1,
*                              "type_3": 1,
*                              "type_4": 1,
*                              "type_5": 0,
*                              "type_6": 0,
*                              "type_7": 0
*                          },
*                          "pre_show_ids": [
*                              1,
*                              2,
*                              3,
*                              4
*                          ],
*                          "total_reactions": 4,
*                          "current_user_reactions": [
*                              1,
*                              2,
*                              3,
*                              4
*                          ]
*                      },
*                      "created_at": "2020-11-01 16:37:17"
*                  },
*                  {
*                      "id": 3,
*                      "content": {
*                          "Date": "09 April 2020",
*                          "Question": "Comment Question"
*                      },
*                      "user_id": 1,
*                      "card_deck_id": 50,
*                      "user_name": "super 001",
*                      "user_avatar": {
*                          "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
*                          "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
*                      },
*                      "reactions": {
*                          "list_reactions": {
*                              "type_1": 0,
*                              "type_2": 0,
*                              "type_3": 0,
*                              "type_4": 0,
*                              "type_5": 0,
*                              "type_6": 0,
*                              "type_7": 0
*                          },
*                          "pre_show_ids": [],
*                          "total_reactions": 0,
*                          "current_user_reactions": []
*                      },
*                      "created_at": "2020-11-01 16:37:25"
*                  },
*                  {
*                      "id": 4,
*                      "content": {
*                          "Date": "09 April 2020",
*                          "Question": "Comment Question"
*                      },
*                      "user_id": 2,
*                      "card_deck_id": 50,
*                      "user_name": "super 002",
*                      "user_avatar": {
*                          "original": "",
*                          "icon": ""
*                      },
*                      "reactions": {
*                          "list_reactions": {
*                              "type_1": 0,
*                              "type_2": 0,
*                              "type_3": 0,
*                              "type_4": 0,
*                              "type_5": 0,
*                              "type_6": 0,
*                              "type_7": 0
*                          },
*                          "pre_show_ids": [],
*                          "total_reactions": 0,
*                          "current_user_reactions": []
*                      },
*                      "created_at": "2020-11-01 16:38:20"
*                  },
*                  {
*                      "id": 91,
*                      "content": {
*                          "Date": "09 April 2020",
*                          "Question": "Comment Question 😃"
*                      },
*                      "user_id": 1,
*                      "card_deck_id": 50,
*                      "user_name": "super 001",
*                      "user_avatar": {
*                          "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
*                          "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
*                      },
*                      "reactions": {
*                          "list_reactions": {
*                              "type_1": 0,
*                              "type_2": 0,
*                              "type_3": 0,
*                              "type_4": 0,
*                              "type_5": 0,
*                              "type_6": 0,
*                              "type_7": 0
*                          },
*                          "pre_show_ids": [],
*                          "total_reactions": 0,
*                          "current_user_reactions": []
*                      },
*                      "created_at": "2020-11-23 03:57:43"
*                  }
*              ],
*              "meta": {
*                  "pagination": {
*                      "total": 6,
*                      "count": 5,
*                      "per_page": 5,
*                      "current_page": 1,
*                      "total_pages": 2,
*                      "links": {
*                          "next": "http://app-api.tarovn.net/api/comment?per_page=5&card_deck_id=50&user_id=1&api_key=JuHBNegalcfLXhEMqcFW&page=2"
*                      }
*                  }
*              }
*          },
*          "message": "Success",
*          "errors": null
*      }
*  }
*/

router.route('/card-deck/:id/comments')
  .post(authorize([roles.ADMIN, roles.READER, roles.USER]), validate(createComment), commentController.newComment)
/**
 * @api {post} v1/wiki/card-deck/:id/comments Create card deck comment
 * @apiDescription Create a new card deck
 * @apiVersion 1.0.0
 * @apiName Create Card Deck Comment
 * @apiGroup Wiki
 *
 * @apiParam  {String} msg Card deck comment
 *
 * @apiParamExample {json} Request-Example:
 *  {
 *      "msg": "Admin test comment card deck"
 *  }
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *        "message": "SUCCESS",
 *        "error": null,
 *        "data": {
 *            "status": true,
 *            "data": {
 *                "id": 110,
 *                "user_id": "1",
 *                "card_deck_id": "30",
 *                "content": {
 *                    "msg": "Admin test comment card deck",
 *                    "created_at": "2020-12-28T01:24:42Z"
 *                },
 *                "user_name": "super 001",
 *                "user_avatar": {
 *                    "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
 *                    "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
 *                },
 *                "created_at": "2020-12-28 01:23:15"
 *            },
 *            "message": "Success",
 *            "errors": null
 *        }
 *    }
 */

router.route('/comments/:id/edit')
  .put(authorize([roles.ADMIN, roles.READER, roles.ADMIN]), validate(editComment), commentController.editComment)
/**
 * @api {put} v1/wiki/comments/{comment_id}/edit Update specify comment card deck
 * @apiDescription Update wiki comment card deck
 * @apiVersion 1.0.0
 * @apiName Update wiki comment card deck
 * @apiGroup Wiki
 *
 * @apiParam  {String} msg Card deck comment
 * @apiParam  {Timestamp} msg Card deck created At after created a comment
 *
 * @apiParamExample {json} Request-Example:
 *  {
 *      "msg": "Admin updated comment card deck",
 *      "created_at": "2020-12-25T16:05:31Z"
 *  }
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *  {
 *      "message": "SUCCESS",
 *      "error": null,
 *      "data": {
 *          "status": true,
 *          "data": {
 *              "id": 110,
 *              "content": {
 *                  "msg": "Admin updated comment card deck",
 *                  "created_at": "2020-12-25T16:05:31Z"
 *              },
 *              "user_id": 1,
 *              "card_deck_id": 30,
 *              "user_name": "super 001",
 *              "user_avatar": {
 *                  "original": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/original/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg",
 *                  "icon": "https://taro-app-v2.s3.ap-southeast-1.amazonaws.com/avatar/icon/oF1nSRaoksAbUM2MDpKsBnWL5yoVtSzv9z8OTLu2.jpeg"
 *              },
 *              "reactions": {
 *                  "list_reactions": {
 *                      "type_1": 0,
 *                      "type_2": 0,
 *                      "type_3": 0,
 *                      "type_4": 0,
 *                      "type_5": 0,
 *                      "type_6": 0,
 *                      "type_7": 0
 *                  },
 *                  "pre_show_ids": [],
 *                  "total_reactions": 0,
 *                  "current_user_reactions": []
 *              },
 *              "created_at": "2020-12-28 01:23:15"
 *          },
 *          "message": "Success",
 *          "errors": null
 *      }
 *  }
 */

router.route('/comments/:id')
  .delete(authorize([roles.ADMIN, roles.READER, roles.ADMIN]), commentController.deleteComment)
/**
 * @api {delete} v1/wiki/comments/{comment_id} Detele specify wiki comment card deck
 * @apiDescription Delete wiki comment card deck
 * @apiVersion 1.0.0
 * @apiName Delete wiki comment card deck
 * @apiGroup Wiki
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *  {
 *      "message": "SUCCESS",
 *      "error": null,
 *      "data": null
 *  }
 */

module.exports = router;
