const express = require('express');
const controller = require('../../controllers/suit.controller');
const validate = require('express-validation');
const {
  createSuit,
} = require('../../validations/suit.validation');
const { authorize } = require('../../middlewares/auth');
const { roles } = require('../../../config/vars')

const router = express.Router();
router
  .route('/')
  /**
   * @api {get} v1/suits List Suits
   * @apiDescription Get a list of suits
   * @apiVersion 1.0.0
   * @apiName ListSuits
   * @apiGroup Suit
   *
   * @apiSuccess {Object[]} suits List of suits.
   */
  .get(authorize([roles.USER]), controller.getAllSuits)
  /**
   * @api {post} v1/suits Create Suit
   * @apiDescription Create a new Suit
   * @apiVersion 1.0.0
   * @apiName CreateSuit
   * @apiGroup Suit
   *
   * @apiParam  {String}             name               Suit's name
   * @apiParam  {String}             [description]      Suit's description
   *
   * @apiSuccess (Created 201) {String}  _id            Suit's id
   * @apiSuccess (Created 201) {String}  name           Suit's name
   * @apiSuccess (Created 201) {String}  description    Suit's description
   * @apiSuccess (Created 201) {Date}    createdAt      Timestamp
   * @apiSuccess (Created 201) {Date}    updatedAt      Timestamp
   *
   * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
   */
  .post(authorize([roles.READER]), validate(createSuit), controller.createSuit);
module.exports = router;
