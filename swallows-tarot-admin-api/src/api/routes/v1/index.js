const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const cardRoutes = require('./card.route');
const suitRoutes = require('./suit.route');
const deckRoutes = require('./deck.route');
const cardDeckRoutes = require('./card_deck.route');
const feedbackRoutes = require('./feedback.route');
const wikiRoutes = require('./wiki.route');
const chatBotRoutes = require('./chat_bot.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK 2'));

/**
 * GET v1/docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/cards', cardRoutes);
router.use('/suits', suitRoutes);
router.use('/decks', deckRoutes);
router.use('/cardDecks', cardDeckRoutes);
router.use('/feedbacks', feedbackRoutes);
router.use('/wiki', wikiRoutes);
router.use('/chat-bot', chatBotRoutes);
module.exports = router;
