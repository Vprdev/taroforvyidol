const Joi = require('joi');

module.exports = {
  createComment: {
    body: {
      msg: Joi.string().required(),
    },
  },
  editComment: {
    body: {
      msg: Joi.string().required(),
      created_at: Joi.date().required()
    },
  },
};
