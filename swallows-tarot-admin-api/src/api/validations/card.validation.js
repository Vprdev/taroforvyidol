const Joi = require('joi');

module.exports = {
  createCard: {
    body: {
      name: Joi.string().required(),
      content: Joi.string(),
      suit_id: Joi.string().required(),
      fortune_telling: Joi.array(),
      keywords: Joi.array(),
      number: Joi.string(),
      meanings_light: Joi.array(),
      meanings_shadow: Joi.array(),
      archetype: Joi.string(),
      hebrew_alphabet: Joi.string(),
      numerology: Joi.string(),
      elemental: Joi.string(),
      mythical_spiritual: Joi.string(),
      astrology: Joi.string(),
      affirmation: Joi.string(),
      questions_to_ask: Joi.array(),
    },
  },
};
