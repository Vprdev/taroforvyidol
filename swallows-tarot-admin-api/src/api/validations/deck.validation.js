const Joi = require('joi');

module.exports = {
  createDeck: {
    body: {
      name: Joi.string().required(),
      description: Joi.string(),
      source: Joi.array(),
      author: Joi.string(),
    },
  },
  updateDeck: {
    body: {
      name: Joi.string().required(),
      description: Joi.string(),
      source: Joi.array(),
      author: Joi.string(),
    },
    params: {
      deckId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    },
  },
};
