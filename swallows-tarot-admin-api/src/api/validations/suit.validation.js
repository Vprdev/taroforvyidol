const Joi = require('joi');

module.exports = {
  createSuit: {
    body: {
      name: Joi.string(),
      description: Joi.string(),
    },
  },
};
