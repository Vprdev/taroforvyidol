const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi)

module.exports = {
  createCardDeck: {
    fields: {
      name: Joi.string().required(),
      card_id: Joi.objectId().required(),
      deck_id: Joi.objectId().required(),
      group_id: Joi.objectId(),
      language: Joi.string().required(),
      quote: Joi.string().allow(''),
      meaning: Joi.array().required(),
      reference_url: Joi.string(),
      isOriginal: Joi.boolean(),
      keyword: Joi.array()
    },
    files: {
      name: Joi.string().regex(/^.+\.(png|jpg|jpeg)$/).required(),
      size: Joi.number().less(20000000).required()
    }
  },
  updateCardDeck: {
    fields: {
      name: Joi.string().required(),
      card_id: Joi.objectId(),
      deck_id: Joi.objectId(),
      language: Joi.string().required(),
      quote: Joi.string().allow(''),
      meaning: Joi.array().required(),
      reference_url: Joi.string(),
      keyword: Joi.array()
    },
    files: {
      name: Joi.string().regex(/^.+\.(png|jpg|jpeg)$/),
      size: Joi.number().less(20000000)
    },
    params: {
      cardDeckId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    },
  },
};
