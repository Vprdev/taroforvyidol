const dbConn = require('../../config/mysql.js')
const { buildWhereCondition } = require('./ultil.js');
const CardDeckSQL = function (cardDeck) {
  this.id              = cardDeck.id
  this.name            = cardDeck.name
  this.card_id         = cardDeck.card_id
  this.deck_id         = cardDeck.deck_id
  this.slug            = cardDeck.slug
  this.meaning         = cardDeck.meaning
  this.advice_position = cardDeck.advice_position
  this.keywords        = cardDeck.keywords
  this.questions       = cardDeck.questions
  this.detail          = cardDeck.detail
  this.contents        = cardDeck.contents
  this.reference_url   = cardDeck.reference_url
  this.quotes          = cardDeck.quotes
  this.deleted_at      = cardDeck.deleted_at
  this.created_at      = cardDeck.created_at
  this.updated_at      = cardDeck.updated_at
}

CardDeckSQL.getList = ({
  page = 1,
  field = 'card_decks.id',
  order = 'ascend',
  columns = ['*'],
  conditions = [],
  number = 10
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  page = page <= 0 ? 1 : page;
  const queryStr = `SELECT ${columns.join(', ')}
    FROM card_decks
    JOIN cards on card_decks.card_id = cards.id
    JOIN decks on decks.id = card_decks.deck_id
    JOIN images ON imageable_type = 'card_deck' AND imageable_id = card_decks.id
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}
    LIMIT ? OFFSET ?`;
  dbConn.query(queryStr, [+number, (page - 1) * +number], (err, data) => {
    if (!err) res(data)
    else {
      console.log(err);
      reject(err)
    }
  })
})

CardDeckSQL.getCountTotal = ({
  field = 'card_decks.id',
  order = 'ascend',
  conditions = [],
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  whereConditions = buildWhereCondition(conditions);
  dbConn.query(
    `SELECT COUNT(*) as total
    FROM card_decks
    JOIN cards ON card_decks.card_id = cards.id
    JOIN decks ON decks.id = card_decks.deck_id
    JOIN images ON imageable_type = 'card_deck' AND imageable_id = card_decks.id
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}`, (err, data) => {
    if (!err) res(data)
    else {
      console.log(err);
      reject(err)
    }
  })
})



CardDeckSQL.findBy = ({
  conditions = [],
  columns = ['*'],
}) => new Promise((res, reject) => {
  whereConditions = buildWhereCondition(conditions);
  const queryStr = `SELECT ${columns.join(', ')}
    FROM card_decks
    JOIN cards on card_decks.card_id = cards.id
    JOIN decks on decks.id = card_decks.deck_id
    JOIN images ON imageable_type = 'card_deck' AND imageable_id = card_decks.id
    ${whereConditions} LIMIT 1`;
  dbConn.query(queryStr, (err, data) => {
    if (!err) res(data)
    else {
      console.log(err);
      reject(err)
    }
  })
})

module.exports = CardDeckSQL
