const dbConn = require('./../../config/mysql.js')
const FeedbackSQL = function (feedback) {
  this.id = feedback.id
  this.user_id = feedback.user_id
  this.title = feedback.title
  this.content = feedback.content
  this.created_at = feedback.created_at
  this.updated_at = feedback.updated_at
}

FeedbackSQL.listFeedbacks = ({
  page = 1,
  field = 'created_at',
  order,
  number = 10
}) => new Promise((res, reject) => {
  const condition = order === 'ascend' ? 'ASC' : 'DESC'
  dbConn.query(`SELECT * 
  FROM feedbacks
  ORDER BY ${field} ${condition}
  LIMIT ? OFFSET ?
  `, [number, (page - 1) * number], (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

module.exports = FeedbackSQL
