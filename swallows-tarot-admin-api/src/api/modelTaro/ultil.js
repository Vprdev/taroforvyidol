const _ = require('lodash');
// const  urlSlug = require('url-slug')
const slugify = require('slugify')
/**
 * Build Where conditions
 * @param {Array} conditions
 * Ex
 *     {
 *           binding: `WHERE ID = 1 and NAME = 'test'`,
 *           operator: 'RAW',
 *           type: 'OR'
 *     }
 *     {
 *           field: `WHERE ID = 1 and NAME = 'test'`,
 *           operator: true,
 *           type: 'AND'
 *     }
 */
module.exports.buildWhereCondition  = (conditions) => {
  var arr = [];
  var whereConditions = '';
  conditions.map((condition, index) => {
    let type = (condition.type ?? '').toUpperCase() == 'OR' ? ' OR' : ' AND';

    //Ignore first condition type
    if (index == 0) {
      type = '';
    }

    //Build raw query or normal query
    if (!((condition.operator ?? '').toUpperCase() == 'RAW')) {
      arr = [...arr, `${type} ${condition.field} ${condition.operator} ${condition.binding}`];
    } else {
      arr = [...arr, `${type} ${condition.binding}`]
    }
  })
  whereConditions = _.isEmpty(arr) ? '' : 'WHERE ' + arr.join('');
  return whereConditions;
}

module.exports.buildSlug = (string) => {
  return slugify(string, {
    lower: true
  });
}

// module.exports.revertSlug = (slugString) => {
//   return urlSlug.revert(slugString)
// }
