const dbConn = require('./../../config/mysql.js');
const moment = require('moment')
const { buildWhereCondition } = require('./ultil.js');

const UserSQL = function (user) {
  this.name = user.name;
  this.first_name = user.first_name;
  this.last_name = user.last_name;
  this.email = user.email;
  this.telephone = user.telephone;
  this.birthday = user.birthday;
  this.signup_ip_address = user.signup_ip_address;
  this.token = user.token || "";
  this.password = user.password;
  this.created_at = user.created_at;
  this.updated_at = user.updated_at;
  this.activated = true;
};

UserSQL.findUserAuth = email => new Promise((((res, reject) => {
  dbConn.query('SELECT * FROM users WHERE telephone = ? OR email = ? LIMIT 1', [email, email], (err, result, fields) => {
    if (!err) res(JSON.parse(JSON.stringify(result)));
    else reject(err);
  });
})));


UserSQL.createUser = user => new Promise((res, reject) => {
  dbConn.query('INSERT INTO users SET ?', user, (err, result, fields) => {
    if (!err) res(JSON.parse(JSON.stringify(result)));
    else reject(err);
  })
});

UserSQL.findUser = id => new Promise((((res, reject) => {
  dbConn.query('SELECT * FROM users WHERE id = ? LIMIT 1', id, (err, result, fields) => {
    if (!err) res(JSON.parse(JSON.stringify(result)));
    else reject(err);
  });
})));

UserSQL.listUsers = ({ page = 1, field = 'updated_at', order, keyword = '', number = 10 }) => new Promise((((res, reject) => {
  const condition = order === 'ascend' ? 'ASC' : 'DESC'
  dbConn.query(`SELECT
    id,
    first_name,
    last_name,
    telephone,
    email,
    taro_role,
    gender,
    created_at,
    updated_at
    FROM users
    WHERE last_name LIKE '%${keyword}%' OR first_name LIKE '%${keyword}%'
    ORDER BY ${field} ${condition}
    LIMIT ${number} OFFSET ?`, (page - 1) * number, (err, result) => {
    if (!err) res(result)
    else reject(err)
  })
})))

UserSQL.countUsers = ({ keyword = '' }) => new Promise((((res, reject) => {
  dbConn.query(`SELECT count(*) as usersNum
  FROM users
  WHERE last_name LIKE '%${keyword}%' OR first_name LIKE '%${keyword}%'`, (err, result) => {
    if (!err) res(result[0].usersNum)
    else reject(err)
  })
})))

UserSQL.setRoleUser = (userId, roleId) => new Promise((((res, reject) => {
  dbConn.query('UPDATE users SET taro_role = ?, updated_at = ? WHERE id = ?', [roleId, moment().format('YYYY-MM-DD HH:mm:ss'), userId], (err, result) => {
    if (!err) res(result)
    else reject(err)
  })
})))

UserSQL.listUsersByIds = (ids) => new Promise((res, reject) => {
  dbConn.query(`SELECT id, name FROM users WHERE id IN (${ids.join(',')})`, (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

UserSQL.listByConditions = ({page = 1, field = 'id', order = 'ascend', columns = ['*'], conditions = [], number = 10
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  page = page <= 0 ? 1 : page;
  dbConn.query(
    `SELECT ${columns.join(', ')}
    FROM users
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}
    LIMIT ? OFFSET ?`, [+number, (page - 1) * +number], (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

UserSQL.getCountTotal = ({field = 'users.id', order = 'ascend', conditions = []
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  dbConn.query(
    `SELECT COUNT(*) as total
    FROM users
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}`, (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

module.exports = UserSQL;
