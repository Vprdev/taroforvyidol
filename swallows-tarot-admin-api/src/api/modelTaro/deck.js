const dbConn = require('../../config/mysql.js')
const { buildWhereCondition } = require('./ultil.js');
const DeckSQL = function (cardDeck) {
  this.id          = cardDeck.id
  this.name        = cardDeck.name
  this.slug        = cardDeck.slug
  this.source      = cardDeck.source
  this.description = cardDeck.description
  this.deleted_at  = cardDeck.deleted_at
  this.created_at  = cardDeck.created_at
  this.updated_at  = cardDeck.updated_at
  this.author      = cardDeck.questions
}
DeckSQL.searchList = ({
  field = 'name',
  order = 'ascend',
  columns = ['*'],
  conditions = [],
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  dbConn.query(
    `SELECT ${columns.join(', ')}
    FROM decks
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}`, (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

DeckSQL.getList = ({
  page = 1,
  field = 'id',
  order = 'ascend',
  columns = ['*'],
  conditions = [],
  number = 10
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  page = page <= 0 ? 1 : page;
  dbConn.query(
    `SELECT ${columns.join(', ')}
    FROM decks
    JOIN images ON imageable_type = 'deck' AND imageable_id = decks.id
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}
    LIMIT ? OFFSET ?`, [+number, (page - 1) * +number], (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

DeckSQL.getCountTotal = ({
  field = 'decks.id',
  order = 'ascend',
  conditions = [],
}) => new Promise((res, reject) => {
  const orderCondition = order === 'ascend' ? 'ASC' : 'DESC'
  const whereConditions = buildWhereCondition(conditions);
  dbConn.query(
    `SELECT COUNT(*) as total
    FROM decks
    JOIN images ON imageable_type = 'deck' AND imageable_id = decks.id
    ${whereConditions}
    ORDER BY ${field} ${orderCondition}`, (err, data) => {
    if (!err) res(data)
    else reject(err)
  })
})

module.exports = DeckSQL
