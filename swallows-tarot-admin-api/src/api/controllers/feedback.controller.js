const response = require('../helpers/apiResponse')
const FeedbackSQL = require('../modelTaro/feedback.model')
const UserSQL = require('../modelTaro/user.model')

exports.getAllFeedbacks = async (req, res, next) => {
  try {
    const feedbacks = await FeedbackSQL.listFeedbacks(req.query)
    const idUsers = new Set(feedbacks.map(item => item.user_id))
    const usersHash = await new Promise(async resolve => {
      const hash = {}
      const users = await UserSQL.listUsersByIds([...idUsers])
      users.forEach(item => {
        hash[item.id] = item.name
      })
      resolve(hash)
    })
    return response.successResponse(res, feedbacks.map(item => ({
      ...item,
      author: usersHash[item.user_id]
    })))
  } catch (error) {
    next(error)
  }
}