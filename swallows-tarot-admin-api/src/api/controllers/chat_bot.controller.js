const ChatService = require('../services/chat.service')
const response = require('../helpers/apiResponse')
const {roles} = require('../../config/vars');

exports.getListMessage = async (req, res, next) => {
  try {
    const userTaroRole = req.user?.taro_role;
    const roomId = ChatService.getChatBotRoomId(req.user?.id);
    // Admin can join any room
    // User cant not join other user's room
    // if (!userTaroRole || (userTaroRole != roles.ADMIN && req.query?.room != roomId)) {
    //   return response.customReponseNoData(res, 403, "You are not allowed to join this room!")
    // }
    const messages = await ChatService.getListMessage(req.query)
    /*
    const messageDb = messages.data;
    
    const hardDb = {
      // content: 'Message 1',
      senderId: 1234,
      username: 'John Doe',
      // avatar: 'assets/imgs/doe.png',
      date: '13 November',
      timestamp: '10:20',
      system: false,
      saved: true,
      distributed: true,
      seen: true,
      deleted: false,
      failure: true,
      disableActions: false,
      disableReactions: false,
      // files: [
      //   {
      //     name: 'My File',
      //     size: 67351,
      //     type: 'png',
      //     audio: true,
      //     duration: 14.4,
      //     url: 'https://firebasestorage.googleapis.com/...',
      //     // preview: 'data:image/png;base64,iVBORw0KGgoAA...',
      //     progress: 88
      //   }
      // ],
      reactions: {
        ":))": [
          1234, // USER_ID
          4321
        ],
        ":))": [
          1234
        ]
      },
      // replyMessage: {
      //   content: 'Reply Message',
      //   senderId: 4321,
      //   files: [
      //     {
      //       name: 'My Replied File',
      //       size: 67351,
      //       type: 'png',
      //       audio: true,
      //       duration: 14.4,
      //       // url: 'https://firebasestorage.googleapis.com/...',
      //       preview: 'data:image/png;base64,iVBORw0KGgoAA...'
      //     }
      //   ]
      // },
    }
    for( index in messageDb) {
      messageDb[index].content = messageDb[index].message;
      delete messageDb[index].message;
      messageDb[index] = { ...messageDb[index], ...hardDb};
    } */
    return response.successResponse(res, messages)
  } catch (error) {
    next(error)
  }
}

exports.getListRoom = async (req, res, next) => {
  try {
    const listRoom = await ChatService.getListRoom(req)
    return response.successResponse(res, listRoom)
  } catch (err) {
    next(err);
  }
}
