const httpStatus = require('http-status');
const { omit } = require('lodash');
const User = require('../models/user.model');
const response = require('../helpers/apiResponse')
const UserSQL = require('../modelTaro/user.model')

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const user = await User.get(id);
    req.locals = { user };
    return next();
  } catch (error) {
    return next(error);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.user.transform());

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req, res) => res.json(req.user.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const user = new User(req.body);
    const savedUser = await user.save();
    res.status(httpStatus.CREATED);
    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Replace existing user
 * @public
 */
exports.replace = async (req, res, next) => {
  try {
    const { user } = req.locals;
    const newUser = new User(req.body);
    const ommitRole = user.role !== 'admin' ? 'role' : '';
    const newUserObject = omit(newUser.toObject(), '_id', ommitRole);

    await user.updateOne(newUserObject, { override: true, upsert: true });
    const savedUser = await User.findById(user._id);

    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = (req, res, next) => {
  const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
  const updatedUser = omit(req.body, ommitRole);
  const user = Object.assign(req.locals.user, updatedUser);

  user.save()
    .then(savedUser => res.json(savedUser.transform()))
    .catch(e => next(User.checkDuplicateEmail(e)));
};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const [sizeDocuments, data] = await Promise.all([
      UserSQL.countUsers(req.query),
      UserSQL.listUsers(req.query)
    ])
    return response.successResponse(res, {
      currentPage: req.query.page || 1,
      sizeDocuments,
      data
    })
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
exports.remove = (req, res, next) => {
  const { user } = req.locals;

  user.remove()
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};

exports.currentUser = (req, res, next) => {
  try {
    const { user } = req
    return response.successResponse(res, user)
  } catch (err) {
    next(err)
  }
}

/**
 * Set role for existing user
 * @public
 */
exports.setRoleUser = async (req, res, next) => {
  try {
    const userId = req.body.user_id;
    const roleId = req.body.role_id;
    await UserSQL.setRoleUser(userId, roleId);
    return response.successWithMessages(res);
  } catch (err) {
    next(err)
  }
};
