const CardDeckService = require('../services/card_deck.service')
const CardService = require('../services/card.service')
const DeckService = require('../services/deck.service')
const response = require('../helpers/apiResponse')
const CardDeck = require('../models/card_deck.model')
const formidable = require('formidable')
const { AWS, S3PATH, S3URL } = require('../../config/aws.js');
const fs = require('fs')
const { createCardDeck, updateCardDeck } = require('../validations/card_deck.validation')
const Joi = require('joi');
const _ = require('lodash')
exports.getAllCardDeck = async (req, res, next) => {
  try {
    const data = await CardDeckService.getAll()
    return response.successResponse(res, data)
  } catch (error) {
    next(error)
  }
}

exports.getCardDecksByPage = async (req, res, next) => {
  try {
    const data = await CardDeckService.getCardDecksByPage(req.query)
    return response.successResponse(res, data)
  } catch (error) {
    next(error)
  }
}

exports.getCardVersions = async (req, res, next) => {
  try {
    const data = await CardDeckService.getCardDeckVersionsById(req.params.cardDeckId)
    return response.successResponse(res, data)
  } catch (error) {
    next(error)
  }
}

exports.get = async (req, res, next) => {
  try {
    const data = await CardDeckService.getOneById(req.params.cardDeckId)
    if (!data) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, data)
  } catch (err) {
    next(err)
  }
}

exports.createCardDeck = async (req, res, next) => {
  try {
    var form = new formidable.IncomingForm();
    const { first_name, last_name, id } = req.user
    const created_by = {
      fullname: first_name + ' ' + last_name,
      user_id: id
    }
    form.parse(req, async (err, fields, files) => {
      if (err) {
        next(err);
        return;
      }
      fields.meaning = JSON.parse(fields.meaning)
      fields.keyword = JSON.parse(fields.keyword || '[]')
      var validator = Joi.validate(fields, createCardDeck.fields);
      if (validator.error != null) {
        return response.customBadRequest(res, validator.error.details);
      }
      if (fields.language === 'en' && fields.isOriginal === true) {
        validator = Joi.validate(files.url != undefined ? { name: files.url.name, size: files.url.size } : {}, createCardDeck.files);
        if (validator.error != null) {
          return response.customBadRequest(res, validator.error.details);
        }
      }
      fields.isOriginal = JSON.parse(fields.isOriginal)
      const existedName = await CardDeckService.getOneByName(fields.name)
      if (existedName) return response.duplicateRecord(res)
      if (fields.language !== 'en' || fields.isOriginal === false) {
        const isDone = await createCardDeckVersion({ ...fields, created_by });
        return isDone ?
          response.successWithMessages(res) :
          response.notFoundRecord(res)
      }
      //Auto gen group_id for EL ver
      delete fields.group_id;
      const [existedCard, existedDeck] = await Promise.all([
        CardService.getOneById(fields.card_id),
        DeckService.getOneById(fields.deck_id)
      ])
      if (!existedCard || !existedDeck) {
        return response.notFoundRecord(res)
      }

      const S3 = new AWS.S3();
      var uploadParams = {
        Bucket: process.env.AWS_BUCKET,
        Body: '',
        Key: '',
        ACL: 'public-read'
      };
      const fileStream = fs.createReadStream(files.url.path);
      fileStream.on('error', function (err) {
        next(err);
      });
      uploadParams.Body = fileStream;
      //Set storage path on S3
      const fileName = (new Date()).getTime() + '-' + files.url.name;
      uploadParams.Key = S3PATH.card_deck + fileName;
      S3.upload(uploadParams, async function (err, data) {
        if (err) {
          next(err);
        }
        await CardDeckService.create({ ...fields, created_by, url: fileName });
        return response.successWithMessages(res)
      });
    });

  } catch (err) {
    next(err)
  }
}

exports.updateCardDeck = async (req, res, next) => {
  try {
    const isExist = await CardDeckService.getOneById(req.params.cardDeckId)
    if (!isExist) return response.notFoundRecord(res)
    var form = new formidable.IncomingForm();
    form.parse(req, async (err, fields, files) => {
      if (err) {
        return next(err);
      }
      fields.meaning = JSON.parse(fields.meaning)
      fields.keyword = JSON.parse(fields.keyword || '[]')
      var validator = Joi.validate(fields, updateCardDeck.fields);
      if (validator.error != null) {
        return response.customBadRequest(res, validator.error.details);
      }
      // check name edited
      const existedName = await CardDeckService.getOneByNameExceptId({ name: fields.name, id: req.params.cardDeckId })
      if (existedName) return response.duplicateRecord(res)
      if (fields.language === 'en') {
        validator = Joi.validate(files.url != undefined ? { name: files.url.name, size: files.url.size } : {}, updateCardDeck.files);
        if (validator.error != null) {
          return response.customBadRequest(res, validator.error.details);
        }

        if (fields.card_id) {
          const isExistCard = await CardService.getOneById(fields.card_id);
          if (!isExistCard) return response.notFoundRecord(res)
        }
        if (fields.deck_id) {
          const isExistDeck = await DeckService.getOneById(fields.deck_id);
          if (!isExistDeck) return response.notFoundRecord(res)
        }
      }

      if (files.url && fields.language === 'en') {
        const S3 = new AWS.S3();
        var uploadParams = {
          Bucket: process.env.AWS_BUCKET,
          Body: '',
          Key: '',
          ACL: 'public-read'
        };
        const fileStream = fs.createReadStream(files.url.path);
        fileStream.on('error', function (err) {
          return next(err);
        });
        uploadParams.Body = fileStream;
        //Set storage path on S3
        const fileName = (new Date()).getTime() + '-' + files.url.name;
        uploadParams.Key = S3PATH.card_deck + fileName;
        S3.upload(uploadParams, async function (err, data) {
          if (err) {
            return next(err);
          }
          console.log(fields)
          const result = await CardDeckService.update(req.params.cardDeckId, { ...fields, url: fileName })
          let params = _.omit(fields, ['language', 'group_id', 'name', 'state'])
          await CardDeckService.updateMany({ group_id: isExist.group_id, language: { "$ne": 'en' } }, { ...params, url: fileName })
          return !result ? response.notFoundRecord(res) :
            response.successResponse(res, result)
        });
      } else {
        //Update for not EL version
        let result
        if (fields.language !== 'en') {
          let params = _.omit(fields, ['language', 'deck_id', 'card_id'])
          result = await CardDeckService.update(req.params.cardDeckId, params)
        } else {
          result = await CardDeckService.update(req.params.cardDeckId, fields)
          let params = _.omit(fields, ['language', 'group_id', 'name', 'state'])
          await CardDeckService.updateMany({ group_id: isExist.group_id, language: { "$ne": 'en' } }, params)
        }
        return !result ? response.notFoundRecord(res) :
          response.successResponse(res, result)
      }
    })
  } catch (err) {
    next(err)
  }
}

exports.delete = async (req, res, next) => {
  try {
    const status = await CardDeckService.deleteOne(req.params.cardDeckId)
    return status ? response.successWithMessages(res) : response.notFoundRecord(res)
  } catch (err) {
    next(err)
  }
}

exports.approve = async (req, res, next) => {
  try {
    const status = await CardDeckService.approve(req.body.ids)
    return status ? response.successWithMessages(res) : response.notFoundRecord(res)
  } catch (err) {
    next(err)
  }
}

const createCardDeckVersion = async (fields) => {
  const [existedCard, existedDeck, existCardDeckByGroup] = await Promise.all([
    CardService.getOneById(fields.card_id),
    DeckService.getOneById(fields.deck_id),
    CardDeck.findOne({ group_id: fields.group_id, language: 'en' }).lean()
  ]);
  if (!existedCard || !existedDeck || !existCardDeckByGroup) {
    return false
  }
  await CardDeckService.create({ ...fields, url: existCardDeckByGroup.url, group_id: fields.group_id });
  return true
}
