const SuitService = require('../services/suit.service')
const response = require('../helpers/apiResponse')

exports.getAllSuits = async (req, res, next) => {
  try {
    const suits = await SuitService.getAll()
    return response.successResponse(res, suits)
  } catch (error) {
    next(error)
  }
}

exports.createSuit = async (req, res, next) => {
  try {
    const existed = await SuitService.getOneById(req.params.suitId)
    if (existed) {
      return response.notFoundRecord(res)
    }
    const suit = await SuitService.create(req.body)
    return response.successResponse(res, suit)
  } catch (err) {
    next(err)
  }
}
