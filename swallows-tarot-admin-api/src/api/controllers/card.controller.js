const CardService = require('../services/card.service')
const response = require('../helpers/apiResponse')

exports.getAllCards = async (req, res, next) => {
  try {
    const cards = await CardService.searchCards(req.query.name)
    return response.successResponse(res, cards)
  } catch (error) {
    next(error)
  }
}

exports.getOneCard = async (req, res, next) => {
  try {
    const card = await CardService.getOneById(req.params.cardId)
    if (!card) {
    return response.notFoundRecord(res)
    }
    return response.successResponse(res, card)
  } catch (err) {
    next (err)
  }
}
