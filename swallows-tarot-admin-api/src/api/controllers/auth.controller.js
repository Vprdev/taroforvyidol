const httpStatus = require('http-status');
const User = require('../models/user.model');
const RefreshToken = require('../models/refreshToken.model');
const PasswordResetToken = require('../models/passwordResetToken.model');
const moment = require('moment-timezone');
const { jwtExpirationInterval, jwtSecret } = require('../../config/vars');
const { omit } = require('lodash');
const APIError = require('../utils/APIError');
const emailProvider = require('../services/emails/emailProvider');
const bcrypt = require('bcryptjs');
const jwt = require('jwt-simple');
const { successResponse, duplicateRecord } = require('../helpers/apiResponse.js')
const response = require('../helpers/apiResponse')

const UserTaro = require('../modelTaro/user.model');
/**
 * Returns a formated object with tokens
 * @private
 */
function generateTokenResponse(user, accessToken = null) {
  const tokenType = 'Bearer';
  const refreshToken = RefreshToken.generate(user).token;
  const expiresIn = moment().add(jwtExpirationInterval, 'days');
  return {
    tokenType,
    accessToken,
    refreshToken,
    expiresIn,
  };
}

/**
 * Returns jwt token if registration was successful
 * @public
 */

exports.register = async (req, res, next) => {
  try {
    const userData = omit(req.body, 'role');
    const user = await new User(userData).save();
    const userTransformed = user.transform();
    const token = generateTokenResponse(user, user.token());
    res.status(httpStatus.CREATED);
    return res.json({ token, user: userTransformed });
  } catch (error) {
    return next(User.checkDuplicateEmail(error));
  }
};

/**
 * Returns jwt token if valid username and password is provided
 * @public
 */
exports.login = async (req, res, next) => {
  try {
    const record = await UserTaro.findUserAuth(req.body.codeID);

    if (record.length <= 0) {
      return res.status(404).json({
        status: false, message: 'RECORD_NOT_FOUND', data: null, error: null,
      });
    }
    if (!bcrypt.compareSync(req.body.password, record[0].password)) {
      return res.status(401)
        .json({
          status: false, message: 'UNAUTHORIZED', data: null, error: null,
        });
    }

    const { token, expires_in } = generateToken(record[0]);

    successResponse(res, { token, token_type: 'Bearer', expires_in })
  } catch (error) {
    return next(error);
  }
};

function formatPhoneNumber(params){
  let phone;
  if(params){
    let firstChar = params.split('')
    if(firstChar[0] == 0){
      firstChar[0] = "+84"
    }
    phone = firstChar.join('').replace(/-/g, '')
  }
  return phone;
}

exports.signup = async (req, res, next) => {
  try {
    const saltRounds = 10;
    const {first_name, last_name, password, telephone} = req.body;
    const formatPhone = formatPhoneNumber(telephone)
    const decodePassword = await bcrypt.hash(password, saltRounds)

    const userRecord = new UserTaro({...req.body, password: decodePassword, telephone: formatPhone, birthday: new Date(), name: `${first_name} ${last_name}`});
    
    let record = await UserTaro.findUserAuth(req.body.email || req.body.telephone);

    if (record.length > 0) return duplicateRecord(res);

    let result = await UserTaro.createUser(userRecord);
    if (result.insertId) {
      let user = await UserTaro.findUser(result.insertId);
      const { name, first_name, last_name, email, telephone, birthday } = user[0];

      successResponse(res, { name, first_name, last_name, email, telephone, birthday })
    }
  }
  catch (error) {
    return next(error);
  }
}

function generateToken(user) {
  const payload = {
    exp: moment().add(jwtExpirationInterval, 'days').unix(),
    iat: moment().unix(),
    sub: user.id,
  };
  return { token: jwt.encode(payload, jwtSecret), expires_in: payload.exp };
}

/**
 * login with an existing user or creates a new one if valid accessToken token
 * Returns jwt token
 * @public
 */
exports.oAuth = async (req, res, next) => {
  try {
    const { user } = req;
    const accessToken = user.token();
    const token = generateTokenResponse(user, accessToken);
    const userTransformed = user.transform();
    return res.json({ token, user: userTransformed });
  } catch (error) {
    return next(error);
  }
};

/**
 * Returns a new jwt when given a valid refresh token
 * @public
 */
exports.refresh = async (req, res, next) => {
  try {
    const { email, refreshToken } = req.body;
    const refreshObject = await RefreshToken.findOneAndRemove({
      userEmail: email,
      token: refreshToken,
    });
    const { user, accessToken } = await User.findAndGenerateToken({ email, refreshObject });
    const response = generateTokenResponse(user, accessToken);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

exports.sendPasswordReset = async (req, res, next) => {
  try {
    const { email } = req.body;
    const user = await User.findOne({ email }).exec();

    if (user) {
      const passwordResetObj = await PasswordResetToken.generate(user);
      emailProvider.sendPasswordReset(passwordResetObj);
      res.status(httpStatus.OK);
      return res.json('success');
    }
    throw new APIError({
      status: httpStatus.UNAUTHORIZED,
      message: 'No account found with that email',
    });
  } catch (error) {
    return next(error);
  }
};

exports.resetPassword = async (req, res, next) => {
  try {
    const { email, password, resetToken } = req.body;
    const resetTokenObject = await PasswordResetToken.findOneAndRemove({
      userEmail: email,
      resetToken,
    });

    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (!resetTokenObject) {
      err.message = 'Cannot find matching reset token';
      throw new APIError(err);
    }
    if (moment().isAfter(resetTokenObject.expires)) {
      err.message = 'Reset token is expired';
      throw new APIError(err);
    }

    const user = await User.findOne({ email: resetTokenObject.userEmail }).exec();
    user.password = password;
    await user.save();
    emailProvider.sendPasswordChangeEmail(user);

    res.status(httpStatus.OK);
    return res.json('Password Updated');
  } catch (error) {
    return next(error);
  }
};

function decodeToken(token) {
  return jwt.decode(token, jwtSecret)
}

exports.logout = async (req, res, next) => {
  try {
    const token = req.header('authorization').split(' ')[1]
    const decoded = decodeToken(token)
    return response.successResponse(res)
  } catch (err) {
    next(err)
  }
}
