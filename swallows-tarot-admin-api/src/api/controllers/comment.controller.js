const CommentService = require('../services/comment.service')
const response = require('../helpers/apiResponse')
const _ = require('lodash')

exports.getList = async (req, res, next) => {
  try {
    const query = req.query;
    query.user_id = req?.user?.id || 1
    const listComments = await CommentService.getListById(req.params.id, req.query)
    if (_.isEmpty(listComments)) {
      return response.successResponse(res, [])
      // return response.notFoundRecord(res)
    }
    return response.successResponse(res, listComments)
  } catch (error) {
    next(error)
  }
}

exports.newComment = async (req, res, next) => {
  try {
    const body = req.body;
    body.user_id = req.user.id
    const comment = await CommentService.createComment(req.params.id, body)
    if (_.isEmpty(comment)) {
      return response.errorServerInternal(res)
    }
    return response.successResponse(res, comment)
  } catch (error) {
    next(error)
  }
}

exports.editComment = async (req, res, next) => {
  try {
    const body = req.body;
    body.user_id = req.user.id
    const comment = await CommentService.editComment(req.params.id, body)
    if (_.isEmpty(comment)) {
      return response.errorServerInternal(res)
    }
    return response.successResponse(res, comment)
  } catch (error) {
    next(error)
  }
}

exports.deleteComment = async (req, res, next) => {
  try {
    const query = req.query;
    query.user_id = req.user.id
    const status = await CommentService.deleteComment(req.params.id, query)
    if (_.isEmpty(status)) {
      return response.errorServerInternal(res)
    }
    return response.successWithMessages(res)
  } catch (error) {
    next(error)
  }
}
