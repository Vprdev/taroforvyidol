const WikiService = require('../services/wiki.service')
const response = require('../helpers/apiResponse')
const _ = require('lodash')

exports.getCardDecks = async (req, res, next) => {
  try {
    const cardDecks = await WikiService.getListCardDecks(req)
    if (_.isEmpty(cardDecks)) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, cardDecks)
  } catch (error) {
    next(error)
  }
}

exports.getDecks = async (req, res, next) => {
  try {
    const decks = await WikiService.getListDecks(req)
    if (_.isEmpty(decks)) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, decks)
  } catch (err) {
    next(err)
  }
}

exports.cardVersion = async (req, res, next) => {
  try {
    if (!isNaN(req.params.id)) {
      card = await WikiService.cardVersionById(req)
    } else {
      const cardSlug = req.params.id;
      card = await WikiService.cardVersionBySlug(cardSlug)
    }
    if (_.isEmpty(card)) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, card)
  } catch (error) {
    next(error)
  }
}

exports.searchDecks = async (req, res, next) => {
  try {
    const decks = await WikiService.searchListDecks(req)
    if (_.isEmpty(decks)) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, decks)
  } catch (err) {
    next(err)
  }
}
