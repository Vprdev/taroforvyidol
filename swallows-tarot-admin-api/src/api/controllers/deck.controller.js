const DeckService = require('../services/deck.service')
const response = require('../helpers/apiResponse')

exports.getAllDecks = async (req, res, next) => {
  try {
    const decks = await DeckService.getAll(req.query.name)
    return response.successResponse(res, decks)
  } catch (error) {
    next(error)
  }
}

exports.getDecksByPage = async (req, res, next) => {
  try {
    const decks = await DeckService.getDecksByPage(req.query)
    return response.successResponse(res, decks)
  } catch (error) {
    next(error)
  }
}

exports.get = async (req, res, next) => {
  try {
    const deck = await DeckService.getOneById(req.params.deckId)
    if (!deck) {
      return response.notFoundRecord(res)
    }
    return response.successResponse(res, deck)
  } catch (err) {
    next(err)
  }
}

exports.createDeck = async (req, res, next) => {
  try {
    const existed = await DeckService.getOneByName(req.body.name)
    if (existed) {
      return response.duplicateRecord(res)
    }
    const newDeck = await DeckService.create(req.body)
    return response.successResponse(res, newDeck)
  } catch (err) {
    next(err)
  }
}

exports.updateDeck = async (req, res, next) => {
  try {
    const result = await DeckService.update(req.params.deckId, req.body)
    const existedName = await DeckService.getOneByNameExceptId({ name: req.body.name, id: req.params.deckId })
    if (existedName) return response.duplicateRecord(res)
    if (!result) {
      return response.notFoundRecord(res)
    } else {
      return response.successResponse(res, result)
    }
  } catch (err) {
    next(err)
  }
}

exports.delete = async (req, res, next) => {
  try {
    const status = await DeckService.deleteOne(req.params.deckId)
    return status ? response.successWithMessages(res) : response.notFoundRecord(res)
  } catch (err) {
    next(err)
  }
}
