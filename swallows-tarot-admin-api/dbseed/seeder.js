/* eslint-disable */
const mongoose = require('../src/config/mongoose');
const Card = require('../src/api/models/card.model');
const Suit = require('../src/api/models/suit.model');
const CardService = require('../src/api/services/card.service')
const SuitService = require('../src/api/services/suit.service')
const mongo = require('mongoose');

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const main = async () => {
  console.log('Clone start');
  try {
    await mongoose.connect();
    await Card.deleteMany({});
    await Suit.deleteMany({});
    const suits = await SuitService.getAllSuitsFromMySQL();
    if (suits.length > 0) {
      const suitsMap = new Map(suits.map((item) => {
        const { id } = item;
        delete item.id;
        item._id = mongo.Types.ObjectId();
        return [id, item];
      }));
      const cards = await CardService.getAllCardsFromMySQL();
      if (cards.length > 0) {
        const newCards = cards.map((item) => {
          const { suit_id } = item;
          delete item.id;
          item._id = mongo.Types.ObjectId();
          item.suit_id = suitsMap.get(suit_id)._id;
          item.fortune_telling = JSON.parse(item.fortune_telling);
          item.keywords = JSON.parse(item.keywords);
          item.meanings_light = JSON.parse(item.meanings_light);
          item.meanings_shadow = JSON.parse(item.meanings_shadow);
          item.questions_to_ask = JSON.parse(item.questions_to_ask);
          return item;
        });
        await Card.insertMany(newCards);
      }
      await Suit.insertMany(Array.from(suitsMap.values()));
    }
    await sleep(1000);
    mongoose.close();
    console.log('Clone end');
    process.exit();
  } catch (err) {
    console.log(err);
  }
};

main();
