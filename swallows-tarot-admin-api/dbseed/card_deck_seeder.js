var seeder = require('mongoose-seed');
const { mongo } = require('../src/config/vars');


let data = require('./card_deck_data_100deck.json');

// Connect to MongoDB via Mongoose
seeder.connect(mongo.uri, function () {

    // Load Mongoose models
    seeder.loadModels([
        'src/api/models/deck.model',
        'src/api/models/card_deck.model'
    ]);

    // // Clear specified collections
    seeder.clearModels(['Deck', 'CardDeck'], function () {

        // Callback to populate DB once collections have been cleared
        seeder.populateModels(data, function () {
            seeder.disconnect();
        });
    });
});
