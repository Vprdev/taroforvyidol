module.exports = {
  apps : [{
    script: './src/index.js',
    watch: false,
    instances : 1,
    cwd: '/home/gitlab/taro/admin-api/current',
    error_file: "/home/gitlab/taro/admin-api/logs/web.err.log",
    out_file: "/home/gitlab/taro/admin-api/logs/web.out.log",
    env: {
      "PORT": 3002,
      "NODE_ENV": 'development'
    },
    env_production: {
      "PORT": 3002,
      "NODE_ENV": 'production'
    }
  }
  ],

  deploy : {
    production : {
      // key:  '~/.ssh/id_rs_gitlab',
      // user : `gitlab`,
      // host: `34.126.161.58`,
      // ref  : 'origin/develop',
      // repo : `git@gitlab.com:swallows/swallows-tarot-admin-api.git`,
      path: "/home/gitlab/taro/admin-api",
      user : `${process.env.SSH_USERNAME}`,
      host : `${process.env.SSH_HOSTMACHINE}`,
      ref  : 'origin/develop',
      repo : `${process.env.GIT_REPOSITORY}`,
      'post-setup': 'yarn install; pm2 start ecosystem.config.js --env production',
      'post-deploy': 'yarn install; yarn prestart; pm2 restart ecosystem.config.js --env production',
      'ssh_options': [
        "StrictHostKeyChecking=no",
        "PasswordAuthentication=no"
      ]
    }
  }
};
