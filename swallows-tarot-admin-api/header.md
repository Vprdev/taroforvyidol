# Overview
## Post data to server
All request form data must be in `json` format.

- Request header `Content-Type`: `application/json`
- Request body content : `json` format.

## Authentication

#### Client
- Call the login api, send params to server
    + username
    + password

#### Server
- Check credential, if OK then generate a `api_token`
- Send `api_token` back to client

When client access the protected endpoints they have to send back `api_token` to server. There are to ways to send:

- Request query: https://api-endpoind?api_token=token_value
- Request header
    + Header key: Authorization
    + Header value: Bearer token_value

## Error response

- `4xx`: client error
- `5xx`: server error

|Status code| Meaning|
|---|---|
|400|Bad request|
|401|Unauthoziration|
|403|Permission denied|
|500|Internal server error|

#### Body response
All response body in case error must follow the format
```json
{
    "message": "The error content message"
}
```
## Chat bot guilde

### Install
- Install `socket.io-client` in client.
- https://socket.io/docs/v3/client-api/

### Configuration

```js
import { io } from 'socket.io-client';

const socket = io(ENDPOINT, {
  transports: ["websocket"],
  auth: {
    user_id: 1 //User id login
  },
});
```

### Implement chatbot

#### Join/Leave Room
**Normal user**
- User open chatbox:
```js
socket.emit('join-room-chat-bot');
```

- User close chatbox:
```js
socket.emit('leave-room-chat-bot');
```

**Admin**
- Admin join a room
```js
socket.emit('join-room-chat-bot', roomId);
```

- Admin leave room:
```js
socket.emit('leave-room-chat-bot', roomId);
```

#### Send/Recieve message
- Send message:
```js
const data = {
  room: `bot-room-user-${userId}`,
  message: chatMsg,
  userId: userId
}
socket.emit('chat-bot-message', data);
```
- Recieve message:
```js
socket.on("chat-bot-message", function (msg) {
  //Handle msg recieved
});
```

- Get list message: see **api-ChatBot-List_messages**
