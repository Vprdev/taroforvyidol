import { io } from 'socket.io-client';

const socket = io('http://127.0.0.1:3000', { 
    transports : ['websocket'] , 
    autoConnect: false,
    auth: {token: localStorage.getItem('access-token')}
});

socket.onAny((event, ...args) => {
  console.log(event, args);
});

export default socket;