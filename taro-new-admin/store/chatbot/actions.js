import ActionTypes from "./action-types";
import MutationTypes from "./mutation-types";

const actions = {
  async [ActionTypes.ROOMS_ALL]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {       
        const response = await this.$axios.$get("/api/chat-bot/rooms", {
          params: payload
        });      
      commit(MutationTypes.SET_ROOMS, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_ROOMS, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },
  async [ActionTypes.CHATBOT_ALL]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    console.log(payload);
    try {       
        const response = await this.$axios.$get("/api/chat-bot/messages?room=bot-room-user-1&page=1&per_page=5", {
          params: payload
        });      
      commit(MutationTypes.SET_CHATBOTS, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_CHATBOTS, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },
};

export default actions;

