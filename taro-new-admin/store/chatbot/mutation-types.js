const MutationTypes = {

  SET_CHATBOTS: "SET_CHATBOTS",
  SET_ROOMS: "SET_ROOMS",
  SET_LOADING: "SET_LOADING"
   
};
  
  export default MutationTypes;
  
