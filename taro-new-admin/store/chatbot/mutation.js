import MutationTypes from "./mutation-types";

const mutations = {
  [MutationTypes.SET_CHATBOTS](state, { data = [] }) {
    state.chatbots = data;
  },
  [MutationTypes.SET_ROOMS](state, { data = [] }) {
    state.rooms = data;
  },
  [MutationTypes.SET_LOADING](state, value) {
    state.loading = value
  }
};

export default mutations;

