import { io } from 'socket.io-client';

class SocketioService {
  socket;
  constructor() {}

  setupSocketConnection() {
    this.socket = io('ws://localhost:3000', {
      transports: ["websocket"],
      auth: {token: localStorage.getItem('access-token')}
    });

    this.socket.emit('my message', 'Hello there from Vue.');
    this.socket.on('my broadcast', (data) => {
      console.log(data);
    });
  }
  disconnect() {
    if (this.socket) {
        this.socket.disconnect();
    }
  }
  
}

export default new SocketioService();