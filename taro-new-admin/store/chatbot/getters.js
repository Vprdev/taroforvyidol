const getters = {
    prefix() {
      return '/chatbot';
    },
    chatbots: state => state.chatbots,
    loading: state => state.loading,
    rooms: state => state.rooms
  }
  
  export default getters
