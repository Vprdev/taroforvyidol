const state = () => ({
  me: {},
  permissions: [],
  canEdit: false,
  canDelete: false,
  loading: false
})

export default state