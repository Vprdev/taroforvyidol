const MutationTypes = {
  SET_TOKEN: "SET_TOKEN",
  SET_LOADING: "SET_LOADING",
  SET_CURRENT_USER: "SET_CURRENT_USER",
  RESET: "RESET"
}

export default MutationTypes;