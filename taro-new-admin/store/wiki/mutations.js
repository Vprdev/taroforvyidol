import MutationTypes from "./mutation-types";

const mutations = {
  [MutationTypes.SET_WIKI_CARD_DECK](state, { data }) {
    state.wikiCardDecks = data
  },

  [MutationTypes.SET_WIKI_DECK](state, { data }) {
    state.wikiDecks = data
  },

  [MutationTypes.SET_WIKI_COMMENT](state, { data }) {
    state.comments = data
  },

  [MutationTypes.SET_LOADING](state, value) {
    state.loading = value
  },

  [MutationTypes.SET_WIKI_SEARCH_DECK](state, { data }) {
    state.wikiSearchDecks = data
  },
}

export default mutations