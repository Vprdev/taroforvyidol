import ActionTypes from "./action-types";
import MutationTypes from "./mutation-types";

const actions = {
  async [ActionTypes.SEARCH_WIKI_DECK]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/wiki/search-decks", {
        params: {
          keywords: payload
        }
      });
      commit(MutationTypes.SET_WIKI_SEARCH_DECK, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_WIKI_SEARCH_DECK, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.GET_WIKI_DECK]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/wiki/decks", {
        params: payload
      });
      commit(MutationTypes.SET_WIKI_DECK, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_WIKI_DECK, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.GET_WIKI_CARD_DECK]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/wiki/card-decks", {
        params: payload
      });
      commit(MutationTypes.SET_WIKI_CARD_DECK, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_WIKI_CARD_DECK, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.GET_COMMENT]({ commit }, { id, ...rest }) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get(`/api/wiki/${id}/comments`, {
        params: rest
      });
      commit(MutationTypes.SET_WIKI_COMMENT, response);
    } catch (err) {
      // handle error
      commit(MutationTypes.SET_WIKI_COMMENT, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },
};

export default actions;
