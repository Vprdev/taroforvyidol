const state = () => ({
  wikiDecks: {},
  wikiCardDecks: {},
  comments: [],
  loading: false,
  wikiSearchDecks: []
});

export default state;
