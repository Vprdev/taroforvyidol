const getters = {
  prefix() {
    return '/wiki';
  },
  wikiDecks: state => state.wikiDecks,
  wikiCardDecks: state => state.wikiCardDecks,
  wikiSearchDecks: state => state.wikiSearchDecks,
  comments: state => state.comments,
  loading: state => state.loading
}

export default getters