const getters = {
  prefix() {
    return '/deck';
  },
  allDecks: state => state.allDecks,
  decks: state => state.decks,
  detail: state => state.detail,
  loading: state => state.loading
}

export default getters