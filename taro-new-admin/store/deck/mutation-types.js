const MutationTypes = {
  SET_DECK_ALL: "SET_DECK_ALL",
  SET_DECK_LIST: "SET_DECK_LIST",
  SET_DECK_DETAIL: "SET_DECK_DETAIL",
  SET_LOADING: "SET_LOADING"
};

export default MutationTypes;
