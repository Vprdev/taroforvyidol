import MutationTypes from "./mutation-types";

const mutations = {
  [MutationTypes.SET_DECK_ALL](state, { data = [] }) {
    state.allDecks = data;
  },

  [MutationTypes.SET_DECK_LIST](state, { data = {} }) {
    state.decks = data;
  },

  [MutationTypes.SET_DECK_DETAIL](state, { data = {} }) {
    state.detail = data;
  },

  [MutationTypes.SET_LOADING](state, value) {
    state.loading = value;
  }
};

export default mutations;
