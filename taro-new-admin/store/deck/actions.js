import ActionTypes from "./action-types";
import MutationTypes from "./mutation-types";

const actions = {
  async [ActionTypes.DECK_ALL]({ commit }) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/decks");
      commit(MutationTypes.SET_DECK_ALL, response);
    } catch (err) {
      commit(MutationTypes.SET_DECK_ALL, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.DECK_LIST]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/decks/pagination", {
        params: payload
      });
      commit(MutationTypes.SET_DECK_LIST, response);
    } catch (err) {
      commit(MutationTypes.SET_DECK_LIST, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.DECK_DETAIL]({ commit }, id) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get(`/api/decks/${id}`);
      commit(MutationTypes.SET_DECK_DETAIL, response);
    } catch (err) {
      // reset detail store
      commit(MutationTypes.SET_DECK_DETAIL, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.DECK_DETAIL_UPDATE](
    { commit },
    { _id, url, __v0, ...rest }
  ) {
    commit(MutationTypes.SET_LOADING, true);
    let response;
    try {
      response = await this.$axios.$put(`/api/decks/${_id}`, {
        ...rest,
        source: [rest.source]
      });
    } catch (err) {
      // reset detail store
    } finally {
      commit(MutationTypes.SET_LOADING, false);
      return response;
    }
  },

  async [ActionTypes.DECK_DELETE](_, id) {
    const response = await this.$axios.$delete(`/api/decks/${id}`);
    return response;
  },

  async [ActionTypes.DECK_CREATE](
    { commit },
    payload
  ) {
    commit(MutationTypes.SET_LOADING, true);
    let response;
    try {
      response = await this.$axios.$post('/api/decks', {
        ...payload,
        source: [payload.source]
      });
    } catch (err) {
      // reset detail store
    } finally {
      commit(MutationTypes.SET_LOADING, false);
      return response;
    }
  },
};

export default actions;
