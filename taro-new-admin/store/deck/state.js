const state = () => ({
  allDecks: [],
  decks: {},
  detail: {},
  loading: false
});

export default state;
