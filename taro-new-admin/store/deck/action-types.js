
const ActionTypes = {
    DECK_ALL: "DECK_ALL",
    DECK_LIST: "DECK_LIST",
    DECK_DETAIL: "DECK_DETAIL",
    DECK_DETAIL_UPDATE: "DECK_DETAIL_UPDATE",
    DECK_DELETE: "DECK_DELETE",
    DECK_CREATE: "DECK_CREATE",
  }
  
  export default ActionTypes;