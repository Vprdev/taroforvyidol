import MutationTypes from "./mutation-types";

const mutations = {
  [MutationTypes.SET_CARD_DECK_ALL](state, { data = {}}) {
    state.allCardDecks = data
  },

  [MutationTypes.SET_CARD_DECK_LIST](state, { data = {}}) {
    state.cardDecks = data
  },

  [MutationTypes.SET_CARD_DECK_DETAIL](state, { data = {} }) {
    state.detail = data;
  },

  [MutationTypes.SET_LOADING](state, value) {
    state.loading = value;
  },

};

export default mutations;