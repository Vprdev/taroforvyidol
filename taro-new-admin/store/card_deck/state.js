const state = () => ({
  allCardDecks: [],
  cardDecks: {},
  detail: {},
  loading: false
});

export default state;
