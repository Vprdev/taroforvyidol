const getters = {
    prefix() {
      return '/card_deck';
    },
    allCardDecks: state => state.allCardDecks,
    cardDecks: state => state.cardDecks,
    detail: state => state.detail,
    loading: state => state.loading
  }
  
  export default getters