import ActionTypes from "./action-types";
import MutationTypes from "./mutation-types";

const actions = {
  async [ActionTypes.CARD_DECK_ALL]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/cards", {
        params: payload
      });
      commit(MutationTypes.SET_CARD_DECK_ALL, response);
    } catch (err) {
      commit(MutationTypes.SET_CARD_DECK_ALL, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.CARD_DECK_LIST]({ commit }, payload) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get("/api/cardDecks/pagination", {
        params: payload
      });
      commit(MutationTypes.SET_CARD_DECK_LIST, response);
    } catch (err) {
      // reset list store
      commit(MutationTypes.SET_CARD_DECK_LIST, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.CARD_DECK_DETAIL]({ commit }, id) {
    commit(MutationTypes.SET_LOADING, true);
    try {
      const response = await this.$axios.$get(`/api/cardDecks/${id}`);
      commit(MutationTypes.SET_CARD_DECK_DETAIL, response);
    } catch (err) {
      // reset detail store
      commit(MutationTypes.SET_CARD_DECK_DETAIL, {});
    } finally {
      commit(MutationTypes.SET_LOADING, false);
    }
  },

  async [ActionTypes.CARD_DECK_DELETE](_, id) {
    const response = await this.$axios.$delete(`/api/cardDecks/${id}`);
    return response;
  },

  async [ActionTypes.CARD_DECK_CREATE](_, payload) {
    response = await this.$axios.$post('/api/cardDecks', payload);
    return response;
  },

  async [ActionTypes.CARD_DECK_UPDATE](_, { id, data }) {
    response = await this.$axios.$put(`/api/cardDecks/${id}`, data);
    return response;
  }
};

export default actions;
